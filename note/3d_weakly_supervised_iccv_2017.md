## Towards 3D Human Pose Estimation in the Wild: a Weakly-supervised Approach

3d, RGB, hourglass, monocular, in-the-wild

### Conference

ICCV 2017

### Association

MSRA

### Code 

pytorch, 3d: https://github.com/xingyizhou/Pytorch-pose-hg-3d

pytorch re-implementation of stacked hourglass, 2d: https://github.com/xingyizhou/pytorch-pose-hg-3d/tree/2D

torch, 3d: https://github.com/xingyizhou/pose-hg-3d

### Note

#### Summary

| Item                | Detail                                                       |
| ------------------- | ------------------------------------------------------------ |
| Goal                | get computers to estimate the 3D poses of humans in the wild. |
| Available Resources | in-the-wild dataset with 2D annotations, in-the-lab dataset with 3D annotations. |
| Solution            | an end-to-end system with a 2D module and a 3D module, mix strong and weak 3D supervision along with a geometry-based regularization. |
| Result              | a network (output: 2D and 3D pose) that exploits the correlation between 2D pose estimation and depth estimation while also taking into account the image. |

#### Why read this paper?

-How to estimate 3d human pose in the wild with weakly-supervised approach, when large dataset is not available?
-How to utilize 2d in-the-wild data and 3d in-the-lab data together for 3d in-the-wild pose estimation?

#### Background & Motivation

Existing datasets for 3d human pose estimation are mainly collected with motion capture devices in controlled environments, namely, large in-the-wild datasets for 3d human pose estimation is not available. However, the best-performing methods are all built upon ConvNets, which are data-hungry. This leads to a major conflict for 3d human pose estimation in the wild. So, current methods can be divided into 3 categories:

1. Split the task into two separate sub-tasks
    For the first sub-task, 2d in-the-wild data is used to train a network that performs well for 2d in-the-wild pose estimation. Existing approaches like Newell et al. ECCv 2016 and existing in-the-wild datasets like MPII and MS COCO can be used. For the second sub-task, which is aimed at infering 3d pose from 2d pose, only 2d joint locations are used as input so it doesn't matter whether the data is in-the-wild or in-the-lab, so in-the-lab datasets like Human3.6m and HumanEva can be used. The major problem is that simply the 2d joint locations seems to be insufficient to be used for lifting the pose from 2d to 3d, as there can be multiple 3d interpretations of a single 2d skeleton.
2. Synthesizing in-the-wild images for training. 
    This strategy seems to be instinctive and good to solve the problem of 3d human pose estimation in the wild: the data is not enough, so we generate more data! But the current result is not ideal as the synthesized images are not distributed as the orginal dataset or can be distinguished from natural images very easily. (More paper to read for this direction)
3. Mixing 2d data with 3d data.
    A 2D-to-3D knowledge transfer approach is also proposed that pre-trains the network with 2d data, as the initialization of 2d data, to boost the performance of 3d pose estimation. This is also the direction of this paper.

![weakly_supervised_3d_iccv_2017](../img/weakly_supervised_3d_iccv_2017.png)

#### Network

##### Architecture

- Start with a batch of images from both 2D (in-the-wild) and 3D (not in-the-wild) datasets.
- Pass the images through some convolutional layers.
- Pass the convolutional features through a stacked hourglass 2D pose estimation module.
  - (Get 2D pose.)
- Pass the 2D joint heatmaps and intermediate 2D features through a depth regression module. 
  - (Get 3D pose.)
  - By involving intermediate features instead of just the output of the 2D module, the 3D module obtains extra semantic information from the image that it can use. There are multiple 3D interpretatios of a single 2D skeleton, so 2D pose isnâ€™t always enough; we want to use image cues too.

![weakly_supervised_3d_network_iccv_2017](../img/weakly_supervised_3d_network_iccv_2017.png)

##### Loss

- The total loss is $L(\hat{Y}_{hm}, \hat{Y}_{depth}|I) = L_{2D}(\hat{Y}_{hm}, Y_{2D})+L_{depth}(\hat{Y}_{depth}|I, Y_{2D})$, where $\hat{Y}_{hm}$ is the predicted heatmaps, $\hat{Y}_{depth}$ is the predicted depth, $I$ is the image, and $Y_{2D}$ is the ground-truth 2D.

- $L_{2D}(\hat{Y}_{hm}, Y_{2D})$ is the 2D loss, and is simply the L2 distance between the predicted heatmaps and the heatmaps rendered from the ground-truth 2D through a Gaussian kernel.

- $L_{depth}(\hat{Y}_{depth}|I,Y_{2D})$ is the depth regression loss, and is an L2 distance between predicted and ground truth if $I$ comes from the 3D dataset, and a special **geometric loss** if $I$ comes from the 2D dataset. 

- The geometric loss is based on the idea that the bone ratios $\frac {l_e } { \overline{l_e} }$ in a person should remain constant, and therefore is defined as the sum of variances of bone ratios within each of four skeleton groups.
  - $l_e$ is the length of bone $e$ in 3D (for one person)
  - $\overline {l_e}$ is the average length of bone $e$ over all 3D skeletons
  - We want to minimize the variance of bone ratios $\frac {l_e } { \overline{l_e} }$


##### Training

- Send a bunch of images in.
- Predict 2D and 3D for all.
- Supervise with 2D loss on the 2D output, and also (for the 3D output)â€¦
  - If the image was originally from the 3D dataset, supervise with L2 loss on real annotations.
  - If the image was originally from the 2D dataset, weakly supervise with geometric 3D loss.

It’s hard to train from scratch. The paper follows a three-stage training scheme – training first the 2D module, then the full network without geometric loss, and finally the full network with geometric loss.

From the holistic view, there's a 2D module, which estimates the pixel coordinates of each joint, connected to a depth regression module, which estimates the mm depth of each joint. The system is trained end-to-end on strong supervision when itâ€™s available, and otherwise on a weak geometric supervision which makes the bone ratios stay the same. The final network is able to predict the 2D and 3D poses of humans in the wild.

#### More to Read

1. *Synthesizing Training Images for Boosting Human 3D Pose Estimation, 3DV 2016*. Synthesize data for 3d human pose estimation.
2. *MoCap-guided Data Augmentation for 3D Pose Estimation in the Wild, NIPS 2016*. Synthesize data for 3d in-the-wild pose estimation, similar to 1. 
3. *Monocular 3D Human Pose Estimation In The Wild Using Improved CNN Supervision, 3DV 2017*. Using 2d data to boost 3d pose estimation in the wild.
