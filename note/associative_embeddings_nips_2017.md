## Associative Embedding: End-to-End Learning for Joint Detection and Grouping

2d, multi-person, RGB, monocular

### Conference

NIPS 2017

### Code 

(tensorflow) https://github.com/umich-vl/pose-ae-demo

(pytorch) https://github.com/umich-vl/pose-ae-train

### Note

#### Background & Motivation

For the bottom-up methods in multi-person pose estimation, usually a two-stage pipeline is used: detect all human joints (keypoints) and then group them into different individuals. But previous grouping methods like deepcut, deepercut, etc., are not ideal because of speed or accuracy. 

To group keypoints, you must either found the connection between keypoints from the same group (the joints on the same person) or that between keypoints and the group (person) it belongs two. This paper chooses the latter path. Instead of using any graph models, it simply trains the network to predit a score (called *tag* or *associative embedding*) between each pixel and its group. So for each pixel, the detection score, which stands for the probability for it to be a keypoint, and the tag, which stands for the group it belongs to. 

#### Idea

Then, why not just accomplishing these two things in one time? Usually, for detection-based pose estimation network, a heatmap, which corresponds to the orginal image pixel-wisely, is the output. if you want both the detection score and the tag for each pixel in the original image, you can simply double the layers of heatmap, which as a result contains both the detection score and the grouping tag. Also a new loss function will be needed for the network to train different tags for pixels in different group. Besides these two changes, you can simply adopt any network used in detection-based pose estimation, say, stacked hourglass (ECCV 2016).

Next, with this modified network, you input a image, and get a detection heatmap as well as a *heatmap* of pixel-wise tag, together. Just like how other methods dealing with heatmap, threshold is applied and only the extremum are taken as detected keypoints. Then find the tags of each pixels and put the points (joints) into the group (person) with same tag value one by one. 

#### Else

##### Multi-Scale

As this paper simply adopts the stacked hourglass network architecture and applies the change of associative embedding, the extraction and fusion of multi-scale feature is neither its focus nor among the contribution. (Stacked hourglass itself has focused on using local and global information to help infer the location of relatively hard-to-detect joints. ) 

However, in the testing (inference) stage, in order to accomodate the different scale of person, input images with different size and resolution are input into the network and the output heatmaps and tags across different scales are combined together. The multi-resolution proves to be effective. 

*However, I still have a problem -- with the multi-scale feature processing of stacked hourglass, why does this network still need the multi-scale evalution? I will look into it.*

##### generalization

This paper actually offers a strategy for many similar problems related to visual unit detection and grouping, for example:

1. Instance segmentation, where the pixel inside an instance is the unit and you need to group them to get the boundaries of instances. 
2. Multi-object tracking, where you detect objects across different video frames and then group them to track. 

This paper offered a single-stage, instead of traditional two-stage, method for these problems. Detection and grouping are done together, especially, for instance segmentation, which I knows a little bit, it seems to be instinctively prefereable. However, the algorithm used in parsing the heatmap to get the final result might need to be changed, as in multi-person pose estimation, you need to deal with just a few keypoints and a simple iterative method is find, but for instance segmentation, each pixel inside an instance is to be grouped and the number is relatively big. 

*Oops! Seems pixel-wise embeddings have been used on semantic segmentation in a paper in ICLR 2016. Check the more to read part.*

#### More to Read 

1. (Semantic segmentation with pixel-wise embeddings) *Learning Dense Convolutional Embeddings for Semantic Segmentation. ICLR 2016*. Read to learn more about the idea and application of embeddings.
2. *Normalized Cut and Image Segmentation. T-PAMI 2000*. (A great and classic paper! I'll read it first. )
