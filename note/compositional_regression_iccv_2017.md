##  Compositional Human Pose Regression

2D, 3D, single-person, RGB, monocular, regression-based

### Conference

ICCV 2017

### Association

MSRA

### Note

The work of MSRA on human pose estimation, lead by Yichen Wei, has been focused on making regression-based methods capable of learning joint dependencies. 

#### Why read this paper?

- How to make the regression-based methods structure-aware?
- How does existing methods benefit from a representation of bones instead of joints?

#### Background & Motivation

Existing approach for (2d) human pose estimation can be seperated into 2 categories, detection-based and regression-based, and the SOTA results are achieved by mostly the former. For detection-based methods, the output heatmap is more expressive of human body structure, but they are hard to be generalized to 3D human pose estimation. For regression-based methods, the output joint coordinates are learned independently and thus the joint denpendencies are not well exploited. However, they are general for both 2D and 3D human pose estimation. So, if the regression-based methods can be made structure-aware, they can be much more valuable. 

#### Idea

A novel bone-based pose representation and loss function is proposed. It better explore joint dependencies and is unified for both 2D and 3D tasks. Also it's complementary to regression-based networks. 

##### Bone-Based Pose Representation

For regression, a joint location is the relative position to the root joint while the bone is relative to the parent joint. Using joint location for regression, the joints are indenpendently estimated, which is not helpful to exloiting the internal structure of human body, and geometric constraints like bone lengths and joint angles are neglected. Using bones for representation, a tree structure of body can be applied and it better satisfies the geometric constaints. 

This is actually a problem of how to represent the location of a joint - globally, with respect to a root joint, or locally, with respect to its parent joint in a kinematic tree. This approach is already discussed in Li and Chan, ACCV 2014.

##### L1-Loss Baseline

L1-loss proves to be a better form for regression-based loss function in experiments compared with L2-loss. In this paper it is adopted as a baseline for comparison. 

##### Compositional Loss Function

A problem for bone-based representation is that the location of joints at the far end are determined by other joints before them along the kinematic tree, and the errors in bones will also be propagated along the tree, which will result in large errors for joints at the far end. 

To solve this problem, long-range losses, besides local losses, are also considered, and they are balanced over the intermediate bones. 

![compositional_loss_function](../img/compositional_loss_function.png)

#### More to Read
1. *3D human pose estimation from monocular images with deep convolutional neural network, ACCV 2014*. Check it for a comparison of the relative joint representation and the kinetic tree. 
1. *Deep Kinematic Pose Regression, ECCV 2016*. The kinematic tree. 
2. *Integral Human Pose Regression, ECCV 2018*. 