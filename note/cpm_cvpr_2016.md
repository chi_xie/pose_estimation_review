## Convolutional Pose Machines

2D, single-person, detection-based

### Conference

CVPR 2016

### Association

CMU

### Code

official caffe (custom) version: https://github.com/shihenw/convolutional-pose-machines-release

unofficial tensorflow version: 
https://github.com/psycharo/cpm


### Note

#### Why read this paper?

- How to implicitly model the spatial and structural information of human pose?

#### Background & Motivation

For articulated human pose estimation, the spatial relationships of body parts are very important information for the inference of joints, especially there're some joints that is occluded, covered by different clothes, etc., which requires the spatial context to get a better result. 

Previous methods like Toshev et al. NIPS 2014 use graphical models to utilize the spatial context and explicitly learns these models. But CNN can learns these information in a implicit way as long as it has a big enough receptive field to cover a region containing different body joints. 

#### Idea

1. Spatial context learning

To extract the spatial context information with CNN, the receptive field of the output layer should be large enough to cover multiple body parts. There are several methods for this: pooling, at the expense of precision, increasing kernel size at the expense of more computation, or increasing the layer at the expense of vanishing gradient. The third, which brings less computation increase, is chosen in this paper.

2. Implicit spatial model

The belief map from the output layer of a stage of network contains the spatial context. Together with the feature map, it is input into next stage to learn to model the long-range dependencies between different body parts. 

3. Sequential prediction

The paper of pose machine proposed a sequential prediction framework to learn rich spatial models. This paper combines pose machine's ability to learn spatial information with the ability of CNN to extract features. In the first stage, the receptive field of the network is relatively small and could only extract local feature on asingle body part. From the second stages, the receptive field is large enough to cover different body parts, and the belief map, together with the feature map, are used as input, and a new belief map, which contains (refined) spatial information, is the output. The location with the maximum value in each part's heatmap is the predicted location of body part. 

4. Intermediate supervision for vanishing gradient

During back propagation, gradients on the layers of early stages may decay to nearly zero. The problem of vanishing gradient arise because the network is too deep. As the CPM is a sequential network composed of several stages of same structure, intermediate supervision can be performed to compute the loss at the output layer of each stage and losses for different stages can by minimized through back propagation together. Note that intermediate supervision is also performed on stacked hourglass, ECCV 2016. 

#### Network Design

In this part I will analyze the overall network design according to both the paper and the [code](https://github.com/shihenw/convolutional-pose-machines-release) released. 

![cpm_network](../img/cpm_network.png)

As this image of network structure is quite self-explanatory, no more long description here. 

Note that:

a. A center map, which is only mentioned slightly in the paper, is also used as input for subsequent stages together with the belief map and feature map. It is  a Gaussian peak indicating center of the primary subject (in this case, the person detected). 

b. In the implementation, 4 stages for upper-body estimation, and 6 stages for full-body estimation. 

c. Start from the third stage, the feature image generated in the process of the previous stage, instead of the original image, is used as input. 

d. All the subsequent stages adopt the same structure. 

e. The full network is differentiable, which allows end-to-end training. 

f. During inference, for each image, images of different scales and resolutions are generated and used as input, and the result is the average of them. 

g. Intermediate supervision is crucial for the training of CPM. 

#### More to Read

1. (pose machine) *Pose Machines: Articulated Pose Estimation via Inference Machines, CVPR 2014*. Read to learn more about pose machine and sequence prediction.
2. *LSTM Pose Machines, CVPR 2018*. Read to learn more about the extension of CPM in video.