## Deep Kinematic Pose Regression

3d, human 3.6m

### Conference 

ECCV 2016

### Association

MSRA

### Note

> Estimating the model parameters instead of joints. the geometric structure is better preserved. however the kinematic model paramererization is highly nonlinear and its optimization in deep networks is hard. also the methods are limited for a fully specified kinematic model(fixed bone lenght, known scale). they do not generalizae to 2d where a good 2d kinematic model does not exist. 
