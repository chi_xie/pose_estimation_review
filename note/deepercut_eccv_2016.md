## DeeperCut: A Deeper, Stronger, and Faster Multi-Person Pose Estimation Model

2d, multi-person, monocular

### Conference

ECCV 2016

### Associan

MPI-Inf

### Website

http://pose.mpi-inf.mpg.de/

### Code

stand-alone part detector: https://github.com/eldar/deepcut-cnn

full model: https://github.com/eldar/deepcut

### Note

