## DeepPose: Human Pose Estimation via Deep Neural Networks

RGB, 2d, monocular, regression-based, single-person

### Conference

CVPR 2014

### Note

#### Why read this paper?

- How to apply CNN for human pose estimation
- Human pose estimation as a regression-based problem

#### Background & Motivation

This paper is one of the first to introduce the application of ConvNets for Human Pose Estimation. AlexNet boost the state-of-the-art in image recognition and this paper tries to adapt it to estimate the keypoints of human body. 

Previous methods use feature detector to detect keypoints, and then a graphical model to explore the relationship between them. Though efficient, these methods only use local feature of body parts, and is restricted by the powerlessness to solve the problem of occlusion. ConvNets, as feature detector, can make use of the holistic views to locate the occluded body parts. 

#### Idea

1. Estimation-as-regression

This paper fomulate human pose estimation as the regression of body part coordiates, which can be produced by the last fully-connected layer of the adapted AlexNet. 

2. Holistic reasoning

For each body joint, the whole image is used as input. The fundamental starting point of this paper is to use CNN for holistic reasoning.

3. Coordinate Normalization

The scale of human body can vary a lot while the predicted coordinates should have a unified coordinate system. Thus a coordinate normalization trick is proposed. The input image is normalized by mean and standard deviation and the output is mapped back to the unnormalized space.

![deeppose](../img/deeppose.png)

#### Problems and Solutions

1. Low accuracy as a result of holistic reasoning

Using the whole image as input, although makes use of the global information to some degree, loses the local information and result in the low accuracy of body part coordinates. 

2. Cascaded regressor to improve accuracy

To improve the accuracy of regression result, a cascade of pose regressors is used. The sub-images around the body part locations, which is the result of first stage, is used as input to another AlexNet for the refinement in the form of regression. However, this solution has two drawbacks: first, to regree the coordinate of all body parts results in a great increase in the computation complexity; second, for some articulated body parts, the groud truth location may be out of the sub-images, which will make the cascaded regressor useless. 

3. Geometric contraints left to be explored well

For Pose Representation and Loss Function: Latter works prove detection-based methods perform better than regression-based methods. The fundamental reason is that regression-based methods simply minimize the per-joint location error independently but ignore the internal structures of the pose. 

For Feature Description: although this paper declares to make use of the strutural information from different body parts, it proves to be not enough. In latter work, either graphical models (like Tompson et al. NIPS 14, Yang et al. CVPR 16) or multi-stage structure capable of multi-scale featuring (Newell et al ECCV 2016, Wei et al CVPR 2016) are used to explicitly or implicitly learn structural information.

#### More to Read

1. *Joint training of a convolutional network and a graphical model for human pose estimation, NIPS 2014*. Detection compared with regression.
2. *Human Pose Estimation with Iterative Error Feedback, CVPR 2016*. A successor in the path of regression-based methods, with multi-stage network to learn the body structure. 
3. *Compositional Human Pose Regression, ICCV 2017*. Regression-based, a different pose representation and loss function which explores the body structure. 
4. Dozens of regression-based 3D human pose estimation methods.