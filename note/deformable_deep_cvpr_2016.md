## End-to-end learning of deformable mixture of parts and deep convolutional neural networks for human pose estimation

2d, monocular, RGB, single-person, detection-based

### Conference

CVPR 2016

### Association

CUHK

### Code

matlab, official: https://github.com/bearpaw/eval_pose

### Website

http://www.ee.cuhk.edu.hk/~wyang/Deep-Deformable-Mixture-of-Parts-for-Human-Pose-Estimation/

### Presentation

The presentation video at CVPR 2016 is quite helpful: https://youtu.be/CC2BDZAMBnA

### Note

CNN + Structured Prediction

#### Why read this paper? 

- How to combine deformable mixture of parts with DCNN?
- How is this different from previous works which also tried to combine DCNN with structure modeling? 
- How the message passing layer is designed to include the geometrical relationship between body parts?

#### Background & Motivation

The work of CUHK in recent years for human pose estimation has been focused on narrowing the gap between deep models and structure modeling. Deep neural networks has been proved to be good at learning feature representations and capturing contextual information. This paper endeavors to use domain prior knowledge such as geometric relationships among body parts to learn better representation. 

Also, before DCNN became popular for human pose estimation, many part-based methods, which utilize graphical models, model the geometical relationship between body parts and achieve good performance, such as Pictorial Struture, Deformable Mixture of Parts, Poselet, etc. It is worth considering to combine these methods into and end-to-end framework. 

#### Idea

To determine the location of body parts, two kinds of information are needed: the apperance of the body part, which is contained in the local feature, and the spatial relationship information from other body parts, which can be learned by spatial models. This paper use a front-end DCNN to learn Part Apperance Terms and some extra message passing layers after the DCNN to learn Spatial Relationship Terms. 

The loss function is 
$$
F(l_i, t_i|I;\theta) = \phi(l_i, t_i|I, \theta) + \psi (l_i, l_j, t_i, t_j | I; \omega ^ {t_i, t_j}_{i,j})
$$
And the overall framework can be shown as

![deformable_deep](../img/deformable_deep.png)

##### Part Apperance Terms

Use a DCNN to predict not only the location, but also the mixture type of each body part. The ground truth mixture type is determined by differnt relative location clusters of a part with regard to its neighbouring parts. Local confidence of the apperance of part $i$ with mixture type $t_i$: 
$$
\phi(l_i, t_i|I, \theta) = log (p(l_i, t_i|I, \theta)) = log(\sigma (f(l_i, t_i|I, \theta)))
$$

##### Spatial Relationship Terms

These message passing layers applies quadratic deformation constraints on the output of front-end DCNN: 
$$
\psi (l_i, l_j, t_i, t_j | I; \omega ^ {t_i, t_j}_{i,j}) = <w_{i,j}^{t_i, t_j}, d(l_i, l_J)>
$$
where
$$
d(l_i, l_j ) = [\Delta x, \Delta x^2, \Delta y, \Delta y^2]
$$
The message passing layer is performed to remove false positive results and correct the wrong part locations. The graphical model here can be either tree structure model or loopy model, depending on the message passing relationships. 

#### Drawbacks

##### Computational Cost

Too much computation cost on the message passing layers.


#### More to Read

1. *Joint training of a convolutional network and a graphical model for human pose estimation, NIPS 2014*. A predecessor in combining DCNN with graphical models. 