## Flowing convnets for human pose estimation in videos

2d, single-person, video, upper-body

### Conference 

ICCV 2015

### Association

ox, vgg

### Code

https://github.com/tpfister/caffe-heatmap

### Note

temporal information of human pose in videos. So the problem is how to utilize the temporal consistence information.

#### Why read this paper?

-How to use optical flow to utilize the temporal information for human pose estimation in videos?

#### Background & Motivation

Compared with human pose estimation based on single RGB image, pose estimation in videos are somewhat different: The temporal information is helpful to solve the problem brought by occlusion and articulation. To think about this in a simple way, even if the current frame is hard for pose estimation, the frames before or after it may be relatively easy, and as the pose cannot change too much in consecutive frames due to temporal consistency, the pose in current frame can be infered based on neighbouring frames. So the difficulty is lowered down, but new challenge arises: how to use the temporal information?

Optical flow is considered to represent the temporal information in video frames pretty well and it is used in this paper. Instead of simply using the optical flow as another source of input to ConvNets, here it is used to warped the pose confidence map in neighbouring frames and then all the frames are combined to infer the pose in the center frame. 

#### Idea

![flowing_convnet](../img/flowing_convnet.png)

##### Heatmap-based Regression

The network structure is shown above.

The network, instead of directly regressing the 2d coordinates, regresses the heatmap, which represent the confidence of joint at each pixel location. Two advantages are proposed: heatmap is good for visualization and can help people understand the thinking process and the failing situation; more suitable for the learning process of multiple joint candidates and avoid the highly non-linear nature of coordinate regression. Of course, this heatmap-based, or detection-based strategy has been proposed by other papers (Tompson et al. NIPS 2014) and is not new here.

![flowing_convnet_heatmap](../img/flowing_convnet_heatmap.png)

##### Spatial Fusion Layers

The paper claimed to use some additional spatial fusion layers to implicitly model the parameters. To achieve this, the output of conv3 and conv7 layers in the original SpatialNet are combined as the input for these layers. Note that the idea of combining the output of multiple layers to get multi-resolution feature is used a lot in later works (Wei et al. CVPR 2016, Newell et al. ECCV 2016). It is not discussed very much why these spatial fusion layers are capable of learning implicit spatial models, but we know that the local feature (from conv3) and relatively global feature (from conv7) are useful for learning spatial dependencies. Besides, the experiments in this paper show that these layers is useful in removing kinematically impossible poses.

![spatial_fusion_layers](../img/spatial_fusion_layers.png)

##### Optical Flow Alignment & Temporal Pooling

As stated before, the optical flow is not used as input but to warp the neighboring frames to the current frame. This proves to be effective in experiments.

![optical_flow_warping](../img/optical_flow_warping.png)

A different feature in their approach is that the warped heatmaps of multiple consecutive frames are not simply averaged, considering that frames at different temporal distances should take different weights. The weights are learned by another ConvNet, which is made up of a 1X1 convolution layer that performs cross-channel pooling. However, as a drawback, these warping and temporal pooling operations make the approach not end-to-end.

#### More to Read

1. *Two-stream convolutional networks for action recognition in videos, NIPS 2014*. Optical flow for action recognition.
2. *Deep convolutional neural networks for efficient pose estimation in gesture videos, ACCV 2014*. A preceding work of this paper.

