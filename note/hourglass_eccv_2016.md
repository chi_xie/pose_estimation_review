## Stacked Hourglass Networks for Human Pose Estimation

monocular, single-person, 2D, detection-based, RGB

### Conference

ECCV 2016

### Association

Umich, Ann Arbor

### Code

training pipeline, torch 7, official: https://github.com/umich-vl/pose-hg-train

code for test and demo: https://github.com/umich-vl/pose-hg-demo

unofficial implementation of pytorch: https://github.com/xingyizhou/pytorch-pose-hg-3d/tree/2D


### Note

It's said that we should read paper with a purpose. 

#### Why read this paper?

- How to extract and combine multi-scale features?
- How to apply the amazing FCN structure to pose estimation?

#### Background & Motivation

For the problem of pose estimation, some body parts can be easy to detect, while others may be difficult and in many situations occluded, which requires the information from other body parts to better infer their location. This means that features corresponds to multiple scales are needed.

Previous methods on pose estimation has tried to extract multi-scale features from image for utilizing the spatial context, like Toshev et al NIPS 2014. The fully convolutional network, which is used for semantic segmentation, also deals with the problem of multi-scale spatial information and has pixel-wise output. So it is reasonable to think about applying a structure similar to FCN on human pose estimation. 

#### Idea

FCN is used to produce pixel-wise output from an image. In order to save the computational cost, it pools down to a very low resolution, which is called the bottom-up part, and then upsamples to the original resolution, which is called top-down. During this process, multi-scale features are generated on different layers. In order to consolidate features of different scales, layers are connected with each other through additional branches.

A single hourglass structure differs from FCN mainly because its bottom-up and top-down part are more symmetrical. For a layer in the bottom-up part, there is a corresponding layer of same scale in the top-down part, which is connected with it through another path. Thus, for a layer in the top-down part, the input comes from the corresponding layer in the bottom-up part, which has the same scale, and the direct predecessor layer, which has a smaller scale. Through the process the multi-scale features are consolidated. 

Just like CPM, a multi-stage network, in which each stage has the same structure, is used for a refinement of the heatmap. That's why it is called *Stacked Hourglass*. As the network is too deep and the same problem of vanishing gradient arises, intermediate supervision is also used here.


#### Network Architecture

As the network of stacked hourglass is used as a basic framework by many later works, it's important to read its code and understand the detailed structure. I read the code of [pytorch version](https://github.com/xingyizhou/pytorch-pose-hg-3d/tree/2D).

In the code, the idea of modularization is reflected and the network is constructed in the way of module --> subnet --> network. 

##### Residual Module

Code for this part: https://github.com/xingyizhou/pytorch-pose-hg-3d/blob/2D/src/models/layers/Residual.py

![residual_module](../img/residual_module.jpg)

The structure of residual module, which is proposed in Kaiming He et al 2015, is shown above. Note that the input size in the third conv layer should be N/2 instead of N.

The first path is made up of 3 conv layers with different filter size, and each comes with Batch Normalization and ReLU. The second path is called identity mapping, which has only one conv layer.

Using residual module allow the network to be really deep and performs still good. It extracts features of higher level (by the first path) and still preserves the information at the original level. It changes the depth of output while preserves the size. It can be regarded as a sophisticated conv layer which preserves the size while used. 

##### Single Hourglass

Code for this part: https://github.com/xingyizhou/pytorch-pose-hg-3d/blob/2D/src/models/hg.py

A hourglass network is the core component in this paper and is made up of residual modules. For each scale, the upper path is on that scale while the lower path goes through the process of up-sampling (max pooling here) and down-samping (nearest-neighbour interpolation here). 

![single_hourglass](../img/single_hourglass.png)

After each down-sampling (downward arrow), the upper path preserves information of that scale; after each up-sampling, the features from different scale are added together. As can be seen here, different scales (4 here) are considered here and thus this method does not requires the multi-scale inferent on one single image like CPM, which make it faster during inference. 

This n-scale (4 here) subnet extract features from original scale to $1/2^n$ scale The size of output is preserved and the depth is changed.

##### Problem of Such Networks

As discussed in Yang et al ICCV 2017, the residual module comes with the problem of activation variance and that is accumulated and that is intensified in the stacked hourglass network.

#### Else

##### Structure Model

Like CPM, the structural and spatial information are learned implicitly through CNN with large enough receptive field, instead of explicitly with a graphical model. This is not the focus of this paper. 

##### Intermediate Supervision

This is also a strategy that's been performed in CPM and is necessary for the training of multiple stacked hourglasses.

#### More to Read

1. (ResNet) *Deep Residual Learning for Image Recognition*. Read it to understand residual module better.
2. (FCN) *Fully Connected Network for Semantic Segmentation*. Read to learn more about the design of FCN.
3. *Bottom-Up and Top-Down Reasoning with Hierarchical Rectified Gaussians, CVPR 2016*. Similar with this paper, read for comparison.
4. *Learning Feature Pyramids for Human Pose Estimation, ICCV 2017*. Scale-invariant feature extraction is proposed and the problem of accumulated activation variance is discussed and solved. 