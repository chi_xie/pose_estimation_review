## Human Pose Estimation with Iterative Error Feedback

RGB, monocular, 2D, single-person, regression-based

### Conference

CVPR 2016

### Association

UCB

### Code

official, with customized caffe: https://github.com/pulkitag/ief

### Note

#### Why read this paper?

- How to iteratively refine the result of keypoint regression

#### Background & Motivation

This paper focus on how to utilize the output of prediction as feedback to refine the result. The idea comes more from other fields than the problems existing in human pose estimation.

Previous works, like DeepPose (Toshev et al. CVPR 2014), directly map input space (image) to output space (2d body joint coordinates) with CNN in a feed-forward way. This paper proposes to relate output space with input space using feedback in order to learn the dependencies in output space. In DeepPose, multi-stage refinement is proposed that only operates on the local predicted keypoint patch. This paper proposes a different refinement strategy by using the output of previous stage, combined with the original image, to adjust the prediction.

#### Idea

##### Error Feedback

A set of predictions is concatenated with the original image as the input for refinement network, and each pass through the network further refines these predictions. Instead of directly predicting the refined result, a correction is predicted for each iteration. The model can be described as
$$
e_t = f(x_t)
$$

$$
y_{t+1} = y_t + e_t
$$

$$
x_{t+1} = I \oplus g(y_{t+1})
$$

As can be seen above, the function $f$, which represents the non-linear transformation of the ConvNet, output a correction $e_t$ for current stage. The predicted result (body joint coordinates here) is refined with the correction. From the predicted coordinates, with a gaussian function $g$ a belief map can be drawn, which is combined with the original image $I$ to be the input for next stage. 

In experiments, predicting error correction proves to be better than directly predicting the refined result. 

![ief_structure](../img/ief_structure.png)

##### Multi-Stage Refinement

It requires multi-stage training and the weights are shared across each iteration. Intermediate supervision is also used here.

**Comparision with CPM** 

Wei et al. CVPR 2016 also use multi-stage training to refine the prediction, but performs intermediate supervision to the joint map directly instead of predicting the error correction like this paper. Altough the error correction proves to be better in this paper and multi-stage refinement is also used, the performance of this method is far less competitive compared with Wei et al. The reason behind this may be the poor expressive power of joint coordinates compared with heatmap, which contains relatively rich spatial information. 

![ief_compare_cpm](../img/ief_compare_cpm.png)

As can be seen in the image, CPM performs intermediate supervision on the detection heatmap, and rich structural information is preserved in the heatmap, making it suitable to be used as input for next stage.

![ief_compare](../img/ief_compare.png)

While in IEF, the map where intermediate supervision is performed is rendered by 2D Gaussian function on the predicted joint coordinates. Thus the spatial information is largely lost and joint dependencies is not well exploited, which kind of explains why IEF is not performing as good as CPM.

#### More to read

1. *Recurrent Models of Visual Attention, NIPS 2014*. Closely related for the part of the use of feedback.
2. *Training a feedback loop for hand poose estimation, ICCV 2015*. Applying feedback to (hand) pose estimation (based on RGB-D image).
3. *Convolutional Pose Machines, CVPR 2016*. Multi-stage refinement, as comparison. 

