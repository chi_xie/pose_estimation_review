## Integral Human Pose Regression

3d, regression-based, one-stage, RGB, monocular

### Conference 

ECCV 2018

### Association

MSRA

### Code

pytorch, official, to be released:

https://github.com/JimmySuen/pytorch-integral-human-pose

### Note

#### More to Read

1. *Human Pose Regression by Combining Indirect Part Detection and Contextual Information, CoRR 2017.* Applying soft-argmax for (2d) human pose estimation.