## Joint training of a convolutional network and a graphical model for human pose estimation

2d, single-person, RGB, monocular, detection-based

### Conference

NIPS 2014

### Association 

NYU 

### Code

An unofficial tensorflow implementation with several modification: https://github.com/max-andr/joint-cnn-mrf

### Note

This paper is one of the first to apply CNN to human pose estimation. It combines a ConvNet with a spatial model and is able to train them jointly, thus achieve state-of-the-art performance on FLIC and LSP datasets. 

#### Why read this paper?

- How to model the spatial relationships between body parts?
- How to combine a graphical model together with a ConvNet and train them jointly?

#### Background & Motivation

Convolutional networks, since DeepPose CVPR 2014, has proved to be useful for the problem of human pose estimation and achieves new SOTA on major benchmarks. 

However, as the low-level mechanics of these networks networks are hard to interpret, it's difficult to apply the part-based models to these methods and structural information is not used very well. This paper tries to find a part-based spatial model that can be conbined with CNN and trained jointly. 

Besides, DeepPose directly use the global information in an image to regress the coordinates of keypoints. Instead, this paper adopt detection-based strategy to output heatmaps that represent the detection score for body parts. Also, to use the local information, a pipeline with multiple banks of different resolutions is used. 

#### Idea

The authors propose a fully-convolutional approach. As input they use multiple images of different resolutions that aim to capture the spatial context of a different size. These images are processed by a series of 5x5 convolutional and max pooling layers. Then the feature maps from different resolutions are added up, followed by 2 large 9x9 convolutions. The final layer with 90x60xK feature maps (where K is the number of joints) is the predicted heatmaps.

![tompson_nips_2014_cnn](../img/tompson_nips_2014_cnn.png)

The CNN-based part detector described above already gives good results, however there are also some anatomical mistakes that potentially can be ruled out by applying a spatial model. 

So the goal is to get rid of such false positives that clearly do not meet kinematic constraints. Traditionally, for such purposes a probabilistic graphical model was used. One of the most popular choices is a tree-structured graphical model, because of the exact inference combined with efficiency due to gaussian pairwise priors, which are most often used. Some approaches combined exact inference with hierarchical structure of a graphical model. Another approaches relied on approximate inference with a loopy graphical model, that allowed to establish connections between symmetrical parts.

An important novelty of this paper is that the spatial model can be modeled as a fully connected graphical model with parameters that can be trained jointly with the part detector. Thus the graphical model can be learned from the data, and there is no need to design it for a specific task and dataset, which is a clear advantage. The schematic description of such spatial model is given below: 

![spatial_model](../img/spatial_model.png)

This high-level spatial model is an MRF-like model over the distribution of spatial locations for each body part. A pair-wise fully connected graph is created and the pair-wise potentials in the graph are computed using convolutional priors, which is the likelihood of a body part occuring at a pixel location given the location of another body part. 

#### Problems 

This paper has the problem that many of its failure are caused by occluded or misattributed limbs. Evaluation at a local scale cannot solve this problem. So the key point is to extract and analyze global feature, which means, multiple resolution required. Later methods like CPM and stacked hourglass are likely to be inspired by this. 

#### More to Read

1. *Learning Human Pose Estimation Features with Convolutional Networks, ICLR 2014*. A previous work from the same group. Read to find more about the hand-crafted spatial model which is similar to the one here. 
2. *Efficient Object Localization Using Convolutional Networks, CVPR 2015*. A later work from the same group. Apply a refinement model which is used to improve the accuracy lost in pooling layer and can be trained jointly with a ConvNet model. 