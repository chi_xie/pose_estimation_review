## Learning Feature Pyramids for Human Pose Estimation

RGB, 2d, single-person, monocular, detection-based

### Conference

ICCV 2017

### Association 

CUHK

### Code

official torch version, based on hourglass: https://github.com/bearpaw/PyraNet

code for the extension on semantic segmentation: https://github.com/wanggrun/Learning-Feature-Pyramids

### Note

This paper pays special attention to the problem of body shape variation and forshortening and proposes a Pyramid Residual Module as the building block of network to solve this. Also, when using stacked hourglass network as the basic structure, a weight initialization scheme for multi-branch network and a solution to solve the problem of variance accumulation is proposed for the training of the network. 

Still, the note start with the problems for which we are looking for answers in this paper.

#### Why read this paper?

- Why is scale-invariant feature detector still important, with all the previous work, and how does this paper achieve that?
- What weight initialization is proposed for multi-branch network?
- How does the paper solve the problem of variance accumulation is stacked hourglass networks?

#### Background & Motivation

Usually for human pose estimation the size of the full body is provided and the DCNNs learn body part detectors from images warped to the right scale based on the right body size. However, the sizes of body parts can be different even when the body sizes are the same, which is the scale variation of body parts due to inter-personal body shape variations and foreshortening caused by viewpoint change and body articulation. Previous methods did not pay special attention to this problem while this paper does. 

Another observation is that the current weight initialization scheme is aimed at plain network and unsuitable for multi-branch networks like stacked hourglass. 

#### Idea

##### Learning Feature Pyramids

A Pyramid Residual Module is proposed in ths paper to solve the above problem. It obtains features of differnent scales via subsampling with different ratios and then upsamples them to the same resolution and sums them together. 

Stacked hourglass is, without doubt, a success in multi-scale feature learning. Some later works, including this one, can be regarded as improvement to this method. The hourglass network is used here, while its building block, the residual module, is replaced by the PRM to achieve scale-invariant feature extraction. 

The PRM is formulated as:
$$
x^{(l+1)} = x^{(l)} + P(x^{(l)}; W^{(l)})
$$
where $P(x^{(l)}; W^{(l)})$ is feature pyramids decomposed as:
$$
P(x^{(l)}; W^{(l)}) = g(\sum_{c=1} ^C f_c (x^{(l)} ; w_{f_c} ^ {(l)}) ; w_g^{(l)} ) + f_0 (x^{(l)} ; w_{f_0} ^ { ( l) })
$$

![PRM](../img/PRM.png)

##### Variance Accumulation

The drawback of residual module is that identity mapping increases the variances of responses when the network goes deeper, which increases the difficulty of optimization. In stacked hourglass network, different output from 2 branches containing the residual module are sumed up, thus increasing the variance. In this paper, 1x1 convolution preceding with batch normalization and ReLU is used to replace the identity mapping when the output of two residual modules are summed up. 

![variance_accumulation](../img/variance_accumulation.png)

#### More to Read

1. *Deep Residual Learning for Image Recognition, CVPR 2015*. Read for more about residual learning.
2. *Identity Mappings in Deep Residual Networks, ECCV 2016*.