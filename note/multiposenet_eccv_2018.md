## MultiPoseNet: Fast Multi-Person Pose Estimation using Pose Residual Networks

2d, RGB, single-view, multi-person, detection-based, bottom-up

### Code

keras+tensorflow, unavailable now

### Note

#### Background & Motivation

Normally, there are two strategies for multi-person pose estimation: bottom-up, which refers to detect all the human joints (keypoints) no matter which person they belong to, and them grouping these joints to form skeletons for the individuals; top-down, using an off-the-shelf object detector to detect human bounding boxes and then perform single-person estimation for each of them. 

*(I dont' want to have the following 2 paragraphs shown here since they're mentioned many times, but they're necessary to make the 2 articles understandable. )*

These two strategies have their own problems. Top-down methods simplified the problems considering there are already many good methods for person detection and single-person estimation, but they are much slower because the time for the second stage is positively correlated to the time. Besides, it's not completely OK to regard multi-person pose estimation as single-person estimation X n. Because the multiple individuals in the image may have interaction, for example, body overlapping, which makes the situation even more complex. Besides, there's the problem called early commitment. 

For bottom-up methods, time is faster but the result is less accurate. Improvements can be made on both the keypoint detection stage and the keypoint grouping stage. Especially, how to group the points into different people? Recent work try to find the relationship between 2 adjacent joints (which is limb), like CMU CVPR 2017 paper (PAF), or the relationship between a joint and the group it belongs to (in Newell et al NIPS 2017 it's the associative embedding which can be thought of as a tag representing the group of a keypoint). 

#### Basic Idea

For the problems above, the paper proposes a multi-task learning method. Though the author claims it to be bottom-up, it seems to be a combination of the above 2 strategies: a keypoint detection subnet to detect all the keypoints and a person detection subnet to find the person bounding boxes. Then, the keypoints in a person's bounding boxes belongs to 1 individual. Thus, no time-consuming stage 2 (single-person pose estimation), which improves the speed; no such difficulty for grouping, which improves the accuracy, sounds great, right? 

Actually, NO. Considering it is very possible for 2 or more individuals to stay very close (and even body overlapping when they are hugging, etc.), the keypoints in a bounding box may come from more than 1 people. So the result can be pretty bad. So this paper proposed a PRN (Pose Residual Network) which is able to filter out the keypoints that come from outsiders and left only the points from the one and only. This is the main contribution of this paper. 

#### PRN in detail

PRN itself is simple a residual multi-layer perceptron (MLP). For each person bouding boxes and the keypoints inside it, the network takes as input the keypoint heatmap of that region $X$ (of course it's resized) and outputs the corresponding heatmap of that person ($Y = \left\{ y_1, y_2, ..., y_k \right\}$). The network can be represented with
$$
y_k = \phi_k(X) + x_k
$$

#### Else

##### Feature Extraction

It is proved in practice that multi-scale feature are important for the problem of human pose estimation: low scale feature for local information and large scale feature for global information which is useful to the inference of relative locations of hard-to-detect joints like knees and ankles. This part is not the focus of this paper and it does not propose any novel network structure for feature extraction, but it applies former methods well -- ResNet as a backbone network for feature extraction, and then 2 Feature Pyramid Network, which is orginally used for object detection, to learn multi-scale feature and output feature pyramids. 

##### Person Segmentation

The task of person instance segmentation is handled together as well. In the keypoint detection subnet, segmentation mask is regarded as an additional heatmap of keypoint detection, and learned together, which results in little additional cost and competitive result. 

#### More to Read

1. RetinaNet (ICCV 2017), which is used for person detection subnet
2. FPN (CVPR 2017), which is used for feature extraction before 2 subnets. Also, the multi-scale pyramidal feature extraction is worth noticing.
3. MLP (multi-layer perceptron)
