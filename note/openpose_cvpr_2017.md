## Realtime Multi-Person 2D Pose Estimation using Part Affinity Fields

### Conference

CVPR 2017

### Association

CMU

### Code

official, caffe: https://github.com/ZheC/Realtime_Multi-Person_Pose_Estimation

official, openpose in C++:

https://github.com/CMU-Perceptual-Computing-Lab/openpose

### Note

existing problems (multi-person):

unknow number of people in a image

interactions between people induce complex spatial inference, due to contact, occlusion, and limb articulations, making association of parts difficult. 

runtime complexity grow with the number of people; hard to be real-time (for top-down).

network structure based on CPM


