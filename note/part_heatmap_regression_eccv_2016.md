## Human pose estimation via Convolutional Part Heatmap Regression

### Conference

ECCV 2016

### Code

official, torch7:   https://github.com/1adrianb/human-pose-estimation

### Website

https://www.adrianbulat.com/human-pose-estimation

### Note

#### Why read this paper?

- How to combine detection with regression for human pose estimation?

#### Background & Motivation

A key problem in human pose estimation is how to extract rich low-level and mid-level feature information, to construct the semantic information between different body parts, and (it will be best) to learn in an end-to-end way. 

In previous studies of human pose estimation, many detection-based methods, (either using ConvNets or not), usually detect possible candidates of body parts, and use graphical models to determine the result according to the spatial relationships (e.g. Tompson et al. NIPS 2014, Yang et al CVPR 2016, Yang et al CVPR 2011). On the other hand, regression-based methods, for example, DeepPose, born to be capable of learning information from holistic view.

In this paper, the authors proposed to combine detection with regression to estimation human pose. The detection part is aimed at learning part feature, and provide information to guide the learning of regression part network, which makes use of holistic information and structral relationship between different parts to detect occluded parts. This can make it capable of solving severe occlusion problem. 

#### Idea

For detection network, usually the output heatmap has lower response score to the occluded parts, which is easy to understand because the information for determine an accurate location is not enough. If the detection part heatmap is inputted into the regression network, the regression network tends to igore these low-score parts, and rely on more global semantic information to infer the correct occluded part locations. The main contribution lies here: the part heatmap is used to guide the learning of global regression. 

For the detection sub-network, the image is used as input, and part detection heatmap is the output. For regression sub-network, the part heatmap is stacked with the original image as input, and the confidence map is the output. 

As for other parts, both VGG-FCN and ResNet, are used in the experiment. The hourglass network is also adopted for the regression sub-network. For the result, this methods just tied-up with Newell et al. ECCV 2016 even when adopting the hourglass sub-network. 

#### More to Read
1. A strange thing is that this paper counts Wei et al. CVPR 2015 (CPM) as regression-based method, which is obviously a detection-based method. Besides, the regression and detection sub-networks both output heatmaps and use pixel-wise loss functions, except cross-entropy loss for detection sub-network and L2 loss for regression sub-network. 
2. No new papers that are strongly related with this paper are mentioned. 