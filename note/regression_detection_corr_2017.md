## Human Pose Regression by Combining Indirect Part Detection and Contextual Information

2d, regression, RGB

### Conference

CoRR 2017

### Code 

keras + tensorflow, python: https://github.com/dluvizon/pose-regression

### Note

#### Why read this paper?

-How to combine the benefit of detection and regression (with a single soft-argmax operation)

#### Background & Motivation

For detection-based methods:
1. Better performance due to the rich information in heatmap. (+)
2. Non-differentiable argmax operation on the heatmap. (-)
3. The precision of predicted keypoints is proportional to that of the heatmap resolution (because the coordinates are discrete and have to be integers). Increased precision means increased memory space and time. (-)
4. Supervision on heatmap requires artificial groud truth generation. (-)

For regression-based methods:
1. Sub-optimal formulation of estimation-by-regression leads to unsatisfactory results. (-)
2. End-to-end and differentiable. (+)
3. Continuous output brings no quantization error. (+)
4. Can be used for both 2D and 3D human pose estimation. (+)

So, if we can reserve the rich expression of heatmap in the process and, instead of performing supervision on the heatmap, learns directly on the predicted joint coordinates, it is likely to be a better option. To do this, there must be a differentiable operation to get joint coordinates from the heatmap to replace the non-differentiable argmax operation.

#### Idea

##### Soft-argmax

The soft-argmax operation takes the "expectation", instead of "maximum" by argmax, from the heatmap to get the coordinates of the expected joint locations. As stated in Sun et al. ECCV 2018, this "taking expectation" operation can be formulated as
$$
J_k = \sum_{p_z = 1} ^ {D} \sum_{p_y = 1}^{H} \sum_{p_x = 1 }^{W} p * H_k (p)
$$
Note that $D=1$ in this paper for 2D heat maps. 

##### Network Structure

The network structure in this paper, following previous studies like Newell et al. ECCV 2016, is composed of multiple stages and designed with the idea of modularization. 

![regression_detection_corr_2017_network](../img/regression_detection_corr_2017_network.png)

The network start with a Stem for basic feature extraction and then several prediction blocks, each composed of a Block A and a Block B. 

Like stacked hourglass, intermediate supervision is performed on each prediction block, but on the coordinates from soft-argmax operation, rather then the heatmap directly. That’s why it’s declared that part detection are performed “indirectly”. 

##### Context Map

It’s declared that contextual information are also used through the context map. However, the context map are generated together with the part detection map and not particular “contextual”. Still, it seems to be useful for the refinement of result. 

#### More to Read

1. *Integral Human Pose Regression, ECCV 2018.* Extension of Soft-argmax to 3d human pose estimation.
