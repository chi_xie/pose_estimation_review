## Sparseness meets deepness: 3D human pose estimation from monocular video

### Conference

CVPR 2016

### Association

UPenn

### Website

https://fling.seas.upenn.edu/~xiaowz/dynamic/wordpress/monocap/

### Code

https://fling.seas.upenn.edu/~xiaowz/dynamic/wordpress/monocap/

### Note

