## Structured Feature Learning for Pose Estimation

RGB, 2d, single-person, single-view, detection-based

### Code

caffe, official: https://github.com/chuxiaoselena/StructuredFeature

### Association

CUHK

### Website

http://www.ee.cuhk.edu.hk/~xgwang/projectpage_structured_feature_pose.html

### Note

#### Why read this paper?

- How to model structural relationships at the feature level?
- The detailed design of geometrical transform kernels.

#### Background & Motivation

This paper, like other works in CUHK, also tries to model the spatial dependencies between body parts, but differently, at the feature level.

To use the spatial relationship between body parts, some existing approch try to construct graphical models on the heatmap predicted by DCNNs. As the information contained in heatmap is quite limited, it leads the author to thinking about a method to model the spatial relationships when extracting features, that is, inside the neural network. 

Existing CNN has proved to be able to learn multi-scale features for both local and global information, and learns geometrical constraints implicitly (e.g. Hourglass ECCV 2016). This method of learning spatial constraints is kind of different as it integrates a tree model into CNN.

#### Idea

##### Framework

As shown in the figure below, an image is input into the network, go through a CNN (this CNN can be VGG, etc.) and several 1X1 convolution to get the feature map for every body part. Then these feature maps are convoluted in both the top-down or bottom-up way in the tree model and the output is the final score map for prediction.

![structured_feature_learning](../img/structured_feature_learning.png)

##### Innovation

1. Geometrical Transform Kernel

![geometrical_transform_kernel](../img/geometrical_transform_kernel.png)

As shown in the image above, both the strength and location of the gaussian distribution in the input map is changed by conv with the kernel. 

In (f), the feature map for lower arm is clear and accurate while in (e) the map for elbow is not, because lower arm is exposed without cloth and easy to detect while different for elbow. How to deal with this problem? 

The kernel can learn to transform the lower arm point to the location of elbow, and by adding the shifted map for lower arm with the map for elbow, other noisy peaks can be removed. So **basing on the assumption that the relative location between body parts are stable**, the kernel learns to perform the transformation of relative locations between different body parts. 

And obviously, the output heatmap with the kernel is much better than without it. 

2. Bi-directional Tree Model

![bi-directional_tree](../img/bi-directional_tree.png)

To model the geometrical relationships between different body parts, the receptive field of the kernel has to be large, which results in lower computation efficiency. To deal with this problem, the author proposes a bi-directional tree model for the information passing in the conv layers with the geometrical transform kernels. 

As shown in the figure above, direct information passing (that is, direct conv with the transform kernel) only happens between adjacent tree nodes (body parts). And indirect information passing models the geometrical relationships between body parts that are not directly connected in the tree. The tree is designed to be bi-directional so that these to information-passing process can be complementary to each other. 

#### Drawbacks

##### Receptive Field of the GTK

To model the relationship between different body parts, the GTK should be large enough to cover different joints. Due to the limit of computation, the GTK has to be smaller and a bi-directional tree is proposed, which is only able to learn the relationship between part pairs. 

##### Unstable Relative Locations

The GTK is based on the assumption that the relative location between different body parts are stable. However, this assumption is not very well validated nor self-evident. There exists a lot of situation when the pose varies a lot and the relative location is quite different, which will make the result non-ideal. This is not discussed in the experiment shown in the paper. 

#### How much is the problem solved?

1. Successfully models the spatial dependencies between body parts. 
2. The proposed tree model can deal with the situation when there are multiple people in the image. 

#### More to Read

1. *Articulated pose estimation by a graphical model with image dependent pairwise relations, NIPS 2014*. The project is largely based on this.
2. *Convolutional Pose Machines, CVPR 2016*. How to model the spatial relationship at the feature level implicitly and how to achieve large receptive field. 