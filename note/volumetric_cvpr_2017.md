## Coarse-to-Fine Volumetric Prediction for Single-Image 3D Human Pose

3d, RGB, single-view

### Conference

CVPR 2017 (spotlight)

### Association 

UPenn

### Code

torch7+matlab, official, demo: https://github.com/geopavlakos/c2f-vol-demo

torch7, official, training: https://github.com/geopavlakos/c2f-vol-train/

### Website

https://www.seas.upenn.edu/~pavlakos/projects/volumetric/

### Note

#### Why read this paper?

-Get to know new strategy for 3D human pose estimation: key point localization in 3D discretized space.

#### Background & Motivation

Previous methods for 3D human pose estimation: 

1. Two-stage: 2D human pose estimation + optimization methods 2D -> 3D. 
Problems of this kind of methods mainly exist in the second stage, which are concluded as reconstruction ambiguity. This is easy to understand: even for a perfectly estimated 2D pose, there could be multiple corrsponding 3D pose, so the information for 2D to 3D reconstruction may be insufficient.

2. End-to-end: estimation-by-regression. 
The input is the image and the output is the 3D joint coordinates. This problem is highly non-linear and the mapping from RGB image to 3D coordinates is hard to learn. 

The problem of regression-based methods also exist for 2D human pose estimation. So the detection-based methods in 2D human pose estimation are where the inspiration comes from. For detection-based methods in 2D pose estimation, the output is a 2D map, where each value (pixel here) stands for the probability at the corresponding coordinate on the 2d image plane. So if we want to apply a similar strategy to 3D pose estimation, then the output should be a 3D volume where each value (voxel here) stands for the probability at the corresponding location in the 3D space. This is how the volumetric representation comes into mind. 

#### Idea

##### Volumetric Representation

This idea is one of the main contributions in this paper, as discussed above, and it has these advantages: 

![volume_representation](../img/volume_representation.png)

1. ConvNets can naturally map 2D images to 3D volumes and this is easier compared with the mapping from 2D images to 3D coordinates, which is highly non-linear. 
2. The mapping can be achieved with a fully convolutional network and non-FC layers, which means the number of parameters can be reduced a lot. 
3. The output is relatively rich compared with direct regression methods, and amenable to other tasks, or pose-processing like pictorial structures optimization to constrain limb lengths or temporal filtering.

##### Coarse-to-fine Pipeline

At first the author tries to construct a iterative prediction network with same component, each performs intermediate supervision on the output 3D volumetric heatmap of size 64X64X64, which is similar to the stacked hourglass network. However, the final result is not ideal. So the author proposed a coarse-to-fine structure, where they maintain the width and height of each stage's output heatmap while increase depth (the number of channels) gradually, considering the depth is the most challenging information to learn.

![coarse_to_fine_volume](../img/coarse_to_fine_volume.png)

#### Improvement

The intermediate supervision is performed on the 3D volume and the groudtruth information is constructed artificially with 3D gaussian, which requires hyperparameter. This means the volume prediction method also have the drawbacks of 2d detection-based methods, as discussed in Sun et al. ECCV 2018 and Luvizon et al. CoRR 2017 (see note [./regression_detection_corr_2017.md](./regression_detection_corr_2017.md)). So if an additional soft-argmax operation is put on each stage's output volume and the intermediate supervision is performed on the 3D coordinates instead of the 3D volume directly, we are likely to see some improvements here.

#### More to Read

1. *A simple yet effective baseline for 3d human pose estimation, ICCV 2017*. Two-stage method and focuses on second stage (mapping 2D pose to 3D).
2. *Sparseness meets deepness: 3D human pose estimation from monocular video, CVPR 2016*. A representative of Two-stage methods.
3. *3D Pictorial Structures for Multiple View Articulated Pose Estimation, CVPR 2013*. Adopt the representation of 3D human pose in a discretized 3D space in multi-view settings. 3D PCP evaluation metric for KTH dataset. 