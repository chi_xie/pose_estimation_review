# Action Recognition

This document contains the reference for a future review on action recognition, probably focusing on pose-based action recognition.

#### An approach to pose-based action recognition

##### Conference 

CVPR 2013



#### P-CNN: Pose-based CNN Features for Action Recognition

##### Conference 

ICCV 2015



#### 2D/3D Pose Estimation and Action Recognition using Multitask Deep Learning

RGB, 2d, 3d, action recognition

#### Conference 

CVPR 2018
