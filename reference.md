# Human Pose Estimation

*Last Update: Oct. 22nd by Chi Xie*

## References

### Papers for 2D

---



#### Fully Convolutional Networks for Semantic Segmentation

semantic segmentation

##### Conference 

CVPR 2015



#### Realtime Multi-Person 2D Pose Estimation using Part Affinity Fields

##### Conference 

CVPR 2017

##### Association

CMU

##### Code

train, caffe, official: https://github.com/ZheC/Realtime_Multi-Person_Pose_Estimation

test, production: https://github.com/CMU-Perceptual-Computing-Lab/openpose

various re-implementation available

##### Note

[./note/openpose_cvpr_2017.md](./note/openpose_cvpr_2017.md)




#### Mask R-CNN

2D, multi-person, RGB, monocular

##### Conference

ICCV 2017

##### Key Point

1. add the mask branch on faster-rcnn. simple and fast.
2. state-of-the-art on COCO keypoint detection

##### Code

https://github.com/facebookresearch/maskrcnn-benchmark

https://github.com/facebookresearch/Detectron



#### Human Pose Estimation with Iterative Error Feedback

RGB, monocular, 2D, single-person, regression-based

##### Conference

CVPR 2016

##### Association

UCB

##### Code

official, with customized caffe: https://github.com/pulkitag/ief

##### Note

[./note/ief_cvpr_2016.md](./note/ief_cvpr_2016.md)



#### Pose Machines: Articulated Pose Estimation via Inference Machines

2d. RGB. single-person, monocular

##### Conference

ECCV 2014

##### Association

CMU

##### Website

http://www.cs.cmu.edu/~vramakri/poseMachines.html




#### Convolutional Pose Machines

2D, single-person, monocular, detection-based

##### Conference

CVPR 2016

##### Code

official caffe (custom) version: https://github.com/shihenw/convolutional-pose-machines-release

unofficial tensorflow version: 
https://github.com/psycharo/cpm


##### Note

[./note/cpm_cvpr_2016.md](./note/cpm_cvpr_2016.md)



#### Stacked Hourglass Networks for Human Pose Estimation

monocular, single-person, 2D, detection-based, RGB

##### Conference

ECCV 2016

##### Association

Umich, Ann Arbor

##### Code

training pipeline, torch 7, official: https://github.com/umich-vl/pose-hg-train

Code for test and demo: https://github.com/umich-vl/pose-hg-demo

unofficial implementation of pytorch: https://github.com/xingyizhou/pytorch-pose-hg-3d/tree/2D

##### Note

[./note/hourglass_eccv_2016.md](./note/hourglass_eccv_2016.md)



#### Associative Embedding: End-to-End Learning for Joint Detection and Grouping

2d, multi-person, RGB, monocular, bottom-up

##### Conference

NIPS 2017

##### Code 

(tensorflow) https://github.com/umich-vl/pose-ae-demo

(pytorch) https://github.com/umich-vl/pose-ae-train

##### Note

[./note/associative_embeddings_nips_2017.md](./note/associative_embeddings_nips_2017.md)



#### DeepPose: Human Pose Estimation via Deep Neural Networks

RGB, 2d, monocular, regression-based, single-person

##### Conference

CVPR 2014

##### Note

[./note/deeppose_cvpr_2014.md](./note/deepose_cvpr_2014.md)




#### Joint action recognition and pose estimation from video

##### Conference

CVPR 2015




#### Combining local appearance and holistic view: Dual-source deep neural networks for human pose

2d, RGB, single-person, monocular

##### Conference

CVPR 2015

cnn

dual-source: object proposals (local feature, from R-CNN) and the full body (global) as input

goal: to capture the local body parts with better semantic meanings in multiple scales.




#### End-to-end learning of deformable mixture of parts and deep convolutional neural networks for human pose estimation

2d, monocular, RGB, single-person, detection-based

##### Conference

CVPR 2016

##### Code

matlab, official: https://github.com/bearpaw/eval_pose

##### Website

http://www.ee.cuhk.edu.hk/~wyang/Deep-Deformable-Mixture-of-Parts-for-Human-Pose-Estimation/


##### Note

[./note/deformable_deep_cvpr_2016.md](./note/deformable_deep_cvpr_2016.md)



#### Joint training of a convolutional network and a graphical model for human pose estimation

2d, single-person, RGB, monocular, detection-based

##### Conference

NIPS 2014

##### Association 

NYU 

##### Code

An unofficial tensorflow implementation with several modification: https://github.com/max-andr/joint-cnn-mrf

##### Note

[./note/joint_training_nips_2014.md](./note/joint_training_nips_2014.md)




#### Learning Feature Pyramids for Human Pose Estimation

RGB, 2d, single-person, monocular, detection-based

##### Conference

ICCV 2017

##### Association 

CUHK

##### Code

official torch version, based on hourglass: https://github.com/bearpaw/PyraNet

code for the extension on semantic segmentation: https://github.com/wanggrun/Learning-Feature-Pyramids

##### Note

[./note/learning_feature_pyramids_iccv_2017.md](./note/learning_feature_pyramids_iccv_2017.md)



#### Hand Keypoint Detection in Single Images using Multiview Bootstrapping

##### Conference 

CVPR 2017



#### Adversarial PoseNet: A Structure-aware Convolutional Network for Human Pose Estimation

RGB, 2d, single-person, monocular, gan

##### Conference

ICCV 2017



#### Towards Accurate Multi-person Pose Estimation in the Wild

2d, monocular, RGB, multi-person, top-down

##### Conference

CVPR 2017

##### Association

google




#### RMPE: Regional Multi-Person Pose Estimation

2d, monocular, RGB, multi-person, top-down

##### Conference

ICCV 2017

##### Association

SJTU, Tencent Youtu

##### Code

train, caffe: https://github.com/Fang-Haoshu/RMPE

torch: https://github.com/MVIG-SJTU/AlphaPose

pytorch: https://github.com/MVIG-SJTU/AlphaPose/tree/pytorch

使用SSD-512检测人体。RMPE方法致力于解决前面提到的early commitment的问题，也就是改善人体检测框的质量。它通过空间变换网络，对同一个人体所产生的不同的裁剪区域（检测框）都变换到使人体在中央，这样就不会对一个人体的不同候选框检测出不同的关键点。



#### Cascaded Pyramid Network for Multi-Person Pose Estimation

2d, multi-person, RGB, monocular, top-down

##### Conference

CVPR 2018

##### Association

megvii

##### Code

official, code and result, in tensorflow.

https://github.com/chenyilun95/tf-cpn

CPN多阶段，globalNet检测简单的关键点，refineNet检测难检测的、有遮挡的关键点。同时，随着网络层数增大， 感受野也增大，有更大面积的context用于判断被遮挡的或者难判断的关键点的位置。

unofficial, pytorch version:

https://github.com/GengDavid/pytorch-cpn



#### DeepCut: Joint Subset Partition and Labeling for Multi Person Pose Estimation

2d, multi-person, monocular

##### Conference

CVPR 2016

##### Associan

MPI-Inf

##### Website

http://pose.mpi-inf.mpg.de/

##### Code

stand-alone part detector: https://github.com/eldar/deepcut-cnn

full model: https://github.com/eldar/deepcut



#### DeeperCut: A Deeper, Stronger, and Faster Multi-Person Pose Estimation Model

2d, multi-person, monocular

##### Conference

ECCV 2016

##### Associan

MPI-Inf

##### Website

http://pose.mpi-inf.mpg.de/

##### Code

stand-alone part detector: https://github.com/eldar/deepcut-cnn

full model: https://github.com/eldar/deepcut

##### Note

[./note/deepercut_eccv_2016.md](./note/deepercut_eccv_2016.md)



#### Articulated pose estimation by a graphical model with image dependent pairwise relations

2d, RGB, monocular, single-person

##### Conference

NIPS 2014

##### Website 

http://www.stat.ucla.edu/~xianjie.chen/projects/pose_estimation/pose_estimation.html

##### Association

UCLA


> cluster detections into typical orientations so that when their classifier makes predictions additional information is available indicating the likely location of a neighboring joint.

use the direction from detection to help locating joints.

> combine a parts-based model with convnets (by using a convnet to learn conditional probabilities for the presence of parts and their spatial relationship with image patches).



#### Parsing Occluded People by Flexible Compositions

2d, RGB, single-person, monocular

##### Conference

CVPR 2015

##### Association

UCLA

##### Website 

http://www.stat.ucla.edu/~xianjie.chen/projects/occluded_people/occluded_people.html



#### Chained Predictions Using Convolutional Neural Networks

2d, rgb, video, monocular, rnn, single-person

##### Conference 

ECCV 2016



#### Human pose estimation via Convolutional Part Heatmap Regression

2d, RGB, single-person, monocular, detection+regression

##### Conference

ECCV 2016

##### Code

official, torch7:   https://github.com/1adrianb/human-pose-estimation

##### Website

https://www.adrianbulat.com/human-pose-estimation

##### Note

[./note/part_heatmap_regression_eccv_2016.md](./note/part_heatmap_regression_eccv_2016.md)




#### LCR-Net: Localization-Classification-Regression for Human Pose

##### Conference 

CVPR 2017

##### Website

https://thoth.inrialpes.fr/src/LCR-Net/




#### Multi-Scale Structure-Aware Network for Human Pose Estimation

RGB, 2d, single-person, single-view



#### Structured Feature Learning for Pose Estimation

RGB, 2d, single-person, monocular, detection-based

##### Code

caffe, official: https://github.com/chuxiaoselena/StructuredFeature

##### Association

CUHK

##### Website

http://www.ee.cuhk.edu.hk/~xgwang/projectpage_structured_feature_pose.html

##### Note

[./note/structured_feature_learning_cvpr_2016.md](./note/structured_feature_learning_cvpr_2016.md)




#### Multi-Context Attention for Human Pose Estimation

RGB, 2d, single-person, monocular, detection-based

##### Conference

CVPR 2017

##### Association 

CUHK

##### Code

torch7 official version: https://github.com/bearpaw/pose-attention



#### Self Adversarial Training for Human Pose Estimation

gan

##### Conference

CVPR 2017 Workshop on Visual Understanding of Humans in Crowd Scene and the 1st Look Into Person (LIP) Challenge



#### A Greedy Part Assignment Algorithm for Real-Time Multi-person 2D Pose Estimation

RGB, 2d, monocular, multi-person, bottom-up

##### Conference

WACV 2018



#### Deeply Learned Compositional Models for Human Pose Estimation

##### Conference

ECCV 2018




#### Human Pose Estimation in Videos

video, 2d, monocular

##### Conference

ICCV 2015



#### Deep Convolutional Neural Networks for Efficient Pose Estimation in Gesture Videos

2d, video, single-person, monocular, regression

##### Conference

ACCV 2014



#### Flowing convnets for human pose estimation in videos

2d, single-person, video, monocular, RGB

##### Conference 

ICCV 2015

##### Association

ox, vgg

##### Code

https://github.com/tpfister/caffe-heatmap

##### Note

[./note/flowing_convnets_iccv_2015.md](./note/flowing_convnets_iccv_2015.md)



#### Personalizing Human Video Pose Estimation

2d, video, monocular

##### Conference 

CVPR 2016 (oral)

##### Association

VGG

##### Code

matlab, official: https://github.com/jjcharles/personalized_pose



#### Thin-Slicing Network: A Deep Structured Model for Pose Estimation in Videos

2d, video, single-person, detection, monocular

##### Conference

CVPR 2017

##### Association

ETH


#### Maximum-Margin Structured Learning With Deep Networks for 3D Human Pose Estimation

3d

##### Conference 

ICCV 2015



#### A Dual-Source Approach for 3D Pose Estimation from a Single Image

3d, RGB, two-stage, Yasin et al. 2016

##### Conference

CVPR 2016

##### Code 

matlab, official: https://github.com/iqbalu/3D_Pose_Estimation_CVPR2016

##### Note

One major challenge for 3D pose estimation from a single RGB image is the acquisition of sufficient training data. In particular, collecting large amounts of training data that contain unconstrained images and are annotated with accurate 3D poses is infeasible. We therefore propose to use two independent training sources. The first source consists of images with annotated 2D poses and the second source consists of accurate 3D motion capture data. To integrate both sources, we propose a dual-source approach that combines 2D pose estimation with efficient and robust 3D pose retrieval. In our experiments, we show that our approach achieves state-of-the-art results and is even competitive when the skeleton structure of the two sources differ substantially.

2d annotated data + 3d mocap data



#### Direct Prediction of 3D Body Poses from Motion Compensated Sequences

3d, video, Tekin et al. 2016

##### Conference

CVPR 2016

##### Note

early linking

motion information from consecutive frames of a video sequence to recover the 3D pose of people. 
Previous approaches typically compute candidate poses in individual frames and then link them in a post-processing step to resolve ambiguities. By contrast, we directly regress from a spatio-temporal volume of bounding boxes to a 3D pose in the central frame. 
We further show that, for this approach to achieve its full potential, it is essential to compensate for the motion in consecutive frames so that the subject remains centered. This then allows us to effectively overcome ambiguities and improve upon the state-of-the-art by a large margin on the Human3.6m, HumanEva, and KTH Multiview Football 3D human pose estimation benchmarks.



#### Learning to Fuse 2D and 3D Image Cues for Monocular Body Pose Estimation

3d, monocular, tekin et al. 2017

##### Conference

ICCV 2017



#### Learning Latent Representations of 3D Human Pose with Deep Neural Networks

3d, rnn, tekin et al. 2018

##### Conference

IJCV 2018

##### Note

We propose an efficient Long-Short-Term-Memory (LSTM) network for enforcing consistency of 3D human pose predictions across temporal windows.



#### Synthesizing Training Images for Boosting Human 3D Pose Estimation

2d->3d, data, RGB, monocular, chen et al. 2016

##### Conference

3DV 2016

##### Code

https://github.com/chen1474147/Deep3DPose

##### Note

3D poses are much harder to annotate, and the lack of suitable annotated training images hinders attempts towards end-to-end solutions. To address this issue, we opt to automatically synthesize training images with ground truth pose annotations. Our work is a systematic study along this road. We find that pose space coverage and texture diversity are the key ingredients for the effectiveness of synthetic training data. We present a fully automatic, scalable approach that samples the human pose space for guiding the synthesis procedure and extracts clothing textures from real images. Furthermore, we explore domain adaptation for bridging the gap between our synthetic training images and real testing photos. We demonstrate that CNNs trained with our synthetic images outperform those trained with real photos on 3D pose estimation tasks.



#### MoCap-guided Data Augmentation for 3D Pose Estimation in the Wild

data, 3d, Rogez and Schmid 2016

##### Conference

NIPS 2016

(Image-based Synthesis for Deep 3D Human Pose Estimation IJCV 2017)

##### Note

lack of training data, generate images with 3d annotations, 
introduce an image-based synthesis engine that artificially augments a dataset of real images with 2D human pose annotations using 3D Motion Capture (MoCap) data.
Given a candidate 3D pose our algorithm selects for each joint an image whose 2D pose locally matches the projected 3D pose. The selected images are then combined to generate a new synthetic image by stitching local image patches in a kinematically constrained manner. The resulting images are used to train an end-to-end CNN for full-body 3D pose estimation. We cluster the training data into a large number of pose classes and tackle pose estimation as a K-way classification problem. Such an approach is viable only with large training sets such as ours. 



#### Monocular 3D Human Pose Estimation In The Wild Using Improved CNN Supervision

3d, monocular, RGB, in-the-wild, mehta et al. 2017

##### Conference

3DV 2017

##### Website

http://gvv.mpi-inf.mpg.de/3dhp-dataset/

##### Note

We propose a CNN-based approach for 3D human body pose estimation from single RGB images that addresses the issue of limited generalizability of models trained solely on the starkly limited publicly available 3D pose data. Using only the existing 3D pose data and 2D pose data, we show state-of-the-art performances on established benchmarks through transfer of learned features, while also generalizing to in-the-wild scenes. We further introduce a new training set for human body pose estimation from monocular images of real humans that has the ground truth captured with a multi-camera marker-less motion capture system. It complements existing corpora with greater diversity in pose, human appearance, clothing, occlusion, and viewpoints, and enables an increased scope of augmentation. We also contribute a new benchmark that covers outdoor and indoor scenes, and demonstrate that our 3D pose dataset shows better in-the-wild performance than existing annotated data, which is further improved in conjunction with transfer learning from 2D pose data. All in all, we argue that the use of transfer learning of representations in tandem with algorithmic and data contributions is crucial for general 3D body pose estimation.


#### Image-based Synthesis for Deep 3D Human Pose Estimation

data, 3d

##### Conference

IJCV 2018



#### Jointly Optimize Data Augmentation and Network Training: Adversarial Data Augmentation in Human Pose Estimation

gan, data

##### Conference 

CVPR 2018



#### Robust Estimation of 3D Human Poses from a Single Image

RGB, 3d, monocular

##### Conference

CVPR 2014

##### Association

PKU



#### Robust 3D Human Pose Estimation from Single Images or Video Sequences

RGB, video, 3d, monocular

##### Conference

T-PAMI 2018

##### Association

PKU



#### Articulated pose estimation with flexible mixtures-of-parts

##### Conference

CVPR 2011



#### Integral Human Pose Regression

3d, regression-based, one-stage, RGB, monocular

##### Conference 

ECCV 2018

##### Association

MSRA

##### Code

pytorch, official, to be released:

https://github.com/JimmySuen/pytorch-integral-human-pose

##### Note

[./note/integral_regression_eccv_2018.md](./note/integral_regression_eccv_2018.md)



#### Human Pose Regression by Combining Indirect Part Detection and Contextual Information

2d, detection+regression, RGB, monocular, single-person

##### Conference

CoRR 2017

this paper is strongly related to the former one (integral human pose estimation)

##### Code 

keras + tensorflow, python: https://github.com/dluvizon/pose-regression

##### Note

[./note/regression_detection_corr_2017.md](./note/regression_detection_corr_2017.md)




#### Towards 3D Human Pose Estimation in the Wild: a Weakly-supervised Approach

3d, RGB, hourglass, regression-based

##### Conference

ICCV 2017

##### Association

MSRA

##### Code 

pytorch, 3d: https://github.com/xingyizhou/Pytorch-pose-hg-3d

pytorch re-implementation of stacked hourglass, 2d: https://github.com/xingyizhou/pytorch-pose-hg-3d/tree/2D

torch, 3d: https://github.com/xingyizhou/pose-hg-3d

##### Note

[./note/3d_weakly_supervised_iccv_2017.md](./note/3d_weakly_supervised_iccv_2017.md)



#### Compositional Human Pose Regression

2D, 3D, single-person, RGB, monocular, regression-based

##### Conference

ICCV 2017

##### Association

MSRA

##### Note

[./note/compositional_regression_iccv_2017.md](./note/compositional_regression_iccv_2017.md)

##### Key Point

1. use bones instead of joints to represent the human body.
2. compositional loss function - well exploit the pose structure.
3. more potential for 3D. Applicable to 2D and 3D, and you can mix 2D and 3D data for training.
4. easy to use and combine with other regression-based method. 

> It only re-parameterizes the pose representation, which is the network output, and enhances the loss function, which relates the output to ground truth. It does not alter other algorithm design choices and is compatible with such choices, such as network architecture. It can be easily adapted into any existing regression approaches with little overhead for memory and computation, in both training and inference.

##### Problem

1. need to know the number of people in the image (this is a common problem of regression bases method. However, it seems that only single person estimation is explored in this paper)



#### Multimodal Deep Autoencoder for Human Pose Recovery

video, 3d

##### Conference

TIP 2015



#### A simple yet effective baseline for 3d human pose estimation

2d->3d, RGB, monocular, two-stage

##### Conference 

ICCV 2017

##### Code 

official, tensorflow: https://github.com/una-dinosauria/3d-pose-baseline

unofficial, pytorch: https://github.com/weigq/3d_pose_baseline_pytorch

##### Association

UBC

##### Note

[./note/simple_baseline_3d_iccv_2017.md](./note/simple_baseline_3d_iccv_2017.md)



#### Human Pose Estimation Using Deep Consensus Voting

##### Conference 

ECCV 2016




#### Monocular 3D Human Pose Estimation by Predicting Depth on Joints

3d, monocular, RGB

##### Conference

ICCV 2017



#### Learning Monocular 3D Human Pose Estimation from Multi-view Images

3d, multi-view, RGB

##### Conference

CVPR 2018

##### Association

EPFL



#### Human Pose Estimation with Parsing Induced Learner

##### Conference

CVPR 2018




#### Learning to Estimate 3D Human Pose and Shape from a Single Color Image

3d, monocular, RGB

##### Conference 

CVPR 2018




#### Recurrent Human Pose Estimation

2d, RGB, rnn

##### Conference

FGR 2017

##### Website

http://www.robots.ox.ac.uk/~vgg/software/keypoint_detection/

##### Code

official, matlab, https://github.com/ox-vgg/keypoint_detection



#### Active Learning for Human Pose Estimation

##### Conference

ICCV 2017



#### Recurrent Network Models for Human Dynamics

human3.6m, video, 3d, rnn

##### Conference 

ICCV 2015

##### Association

UCB



#### Recurrent 3D Pose Sequence Machines

3d, rnn

##### Conference

CVPR 2017 (oral)

##### Code

torch, https://github.com/MudeLin/RPSM

##### Website

http://www.sysu-hcp.net/3d-pose/



#### Deep Kinematic Pose Regression

3d, RGB, regression-based, one-stage, human3.6m

##### Conference 

ECCV 2016

##### Association

MSRA

##### Note

[./note/deep_kinematic_eccv_2016.md](./note/deep_kinematic_eccv_2016.md)



#### Human Pose Estimation using Global and Local Normalization

2d, RGB, single

##### Conference 

ICCV 2017


#### MultiPoseNet: Fast Multi-Person Pose Estimation using Pose Residual Networks

2d, RGB, single-view, multi-person, detection-based, bottom-up

##### Code

keras+tensorflow, unavailable now

##### Note

[./note/multiposenet_eccv_2018.md](./note/multiposenet_eccv_2018.md)



#### Focal Loss for Dense Object Detection (RetinaNet)

object detection, one-stage, coco

##### Conference 

ICCV 2017 (Best Student Paper Award)

##### Association

FAIR

##### Code 

caffe2 official version:
https://github.com/facebookresearch/Detectron

unofficial, keras version: 
https://github.com/fizyr/keras-retinanet



#### Feature Pyramid Networks for Object Detection

object detection, coco

##### Conference 

CVPR 2017



#### Multi-source Deep Learning for Human Pose Estimation

RGB, 2d, single-person, monocular

##### Conference

CVPR 2014



#### Heterogeneous Multi-task Learning for Human Pose Estimation with Deep Convolutional Neural Network

##### Conference

CVPR 2014, IJCV 2015

##### Association

CITYU

##### Website

http://visal.cs.cityu.edu.hk/research/hmlpe/



#### Efficient Human Pose Estimation from Single Depth Images

##### Conference

T-PAMI 2011



#### Strong Appearance and Expressive Spatial Models for Human Pose Estimation

##### Conference

ICCV 2013



#### Pictorial Structures for Object Recognition

##### Conference 

IJCV 2005




#### Progressive Search Space Reduction for Human Pose Estimation

proposed the PCP evaluation metric

##### Conference 

CVPR 2008



#### Bottom-Up and Top-Down Reasoning with Hierarchical Rectified Gaussians

RGB, 2d, single-person

##### Conference

CVPR 2016

##### Code

MatConvNet, offcial: https://github.com/peiyunh/rg-mpii

##### Website

http://peiyunh.github.io/rg-mpii/



#### Efficient Object Localization Using Convolutional Networks

2d, single-person, monocular, RGB, detection-based

##### Conference 

CVPR 2015

##### Association

NYU


#### Modeep: A deep learning framework using motion features for human pose estimation

2d, video, monocular, FLIC Motion Dataset

##### Conferenc

ACCV 2014

##### Association

NYU



#### Learning Human Pose Estimation Features with Convolutional Networks

##### Conference

ICLR 2014

##### Associatioin

NYU




#### LSTM Pose Machines

video, 2d, single-person, monocular, rnn

##### Conference

CVPR 2018

##### Association

SenseTime

##### Code

official, matlab & Opencv: 

https://github.com/lawy623/LSTM_Pose_Machines




#### Deep Residual Learning for Image Recognition

residual module 

##### Conference 

CVPR 2016

##### Association 

MSRA

##### Code

torch, official: https://github.com/facebook/fb.resnet.torch




#### Identity Mappings in Deep Residual Networks

##### Conference 

CVPR 2016

##### Association

MSRA

##### Code

official, torch: https://github.com/KaimingHe/resnet-1k-layers



#### Simple Baselines for Human Pose Estimation and Tracking

2d, multi-person, RGB, video, tracking

##### Conference 

ECCV 2018

##### Association

MSRA

##### Code

pytorch, official, to be released: https://github.com/leoxiaobin/pose.pytorch



#### Multi-Person Pose Estimation with Local Joint-to-Person Associations

RGB, multi-person, 2d

##### Conference 

ECCV 2016, Crowd Understanding Workshop



#### ArtTrack: Articulated Multi-person Tracking in the Wild

RGB, 2d, multi-person, monocular

##### Conference 

CVPR 2017

##### Association

MPI-Inf

##### Code

official, tensorflow: https://github.com/eldar/pose-tensorflow



#### Detect-and-Track: Efficient Pose Estimation in Videos

video

##### Conference

CVPR 2018

##### Association

FAIR

##### Code

https://github.com/facebookresearch/DetectAndTrack

##### Website

https://rohitgirdhar.github.io/DetectAndTrack/

##### Note

1st poseTrack challenge



#### Knowledge-Guided Deep Fractal Neural Networks for Human Pose Estimation

##### Conference

TMM 2017

##### Code

official, caffe: https://github.com/Guanghan/GNet-pose




### Papers for 3D

---

#### 3D Human Pose Estimation from Monocular Images with Deep Convolutional Neural Network

3d, monocular, regression-based, one-stage

##### Conference 

ACCV 2014

##### Association

CITYU



#### 3D Human Pose Estimation from a Single Image via Distance Matrix Regression

3d, RGB, monocular, two-stage, Moreno-Noguer 2017

##### Conference 

CVPR 2017

##### Note

This paper addresses the problem of 3D human pose estimation from a single image. We follow a standard two-step pipeline by first detecting the 2D position of the N body joints, and then using these observations to infer 3D pose. For the first step, we use a recent CNN-based detector. For the second step, most existing approaches perform 2N-to-3N regression of the Cartesian joint coordinates. We show that more precise pose estimates can be obtained by representing both the 2D and 3D human poses using N×N distance matrices, and formulating the problem as a 2D-to-3D distance matrix regression. For learning such a regressor we leverage on simple Neural Network architectures, which by construction, enforce positivity and symmetry of the predicted matrices. The approach has also the advantage to naturally handle missing observations and allowing to hypothesize the position of non-observed joints. Quantitative results on Humaneva and Human3.6M datasets demonstrate consistent performance gains over state-of-the-art. Qualitative evaluation on the images in-the-wild of the LSP dataset, using the regressor learned on Human3.6M, reveals very promising generalization results.



#### Ordinal Depth Supervision for 3D Human Pose Estimation

3d, monocular

##### Association

UPenn

##### Conference 

CVPR 2018 (oral)

##### Code

https://github.com/geopavlakos/ordinal-pose3d

##### Website

https://www.seas.upenn.edu/~pavlakos/projects/ordinal/



#### Keep it SMPL: Automatic Estimation of 3D Human Pose and Shape from a Single Image

3d, monocular, two-stage, human3.6m, Bogo et al. 2016

##### Conference 

ECCV 2015

##### Code

official: https://github.com/genki-ist/simplify

##### Website

http://smplify.is.tue.mpg.de/

2d deepercut + statistical body shape model SPL, projected 3d points compared with detected 2d points (3d to 2d), 







#### Coarse-to-Fine Volumetric Prediction for Single-Image 3D Human Pose

3d, RGB, monocular, detection-based

##### Conference

CVPR 2017 (spotlight)

##### Association 

UPenn

##### Code

torch7+matlab, official, demo: https://github.com/geopavlakos/c2f-vol-demo

torch7, official, training: https://github.com/geopavlakos/c2f-vol-train/

##### Website

https://www.seas.upenn.edu/~pavlakos/projects/volumetric/

##### Note

[./note/volumetric_cvpr_2017.md](./note/volumetric_cvpr_2017.md)



#### 3D Pictorial Structures for Multiple View Articulated Pose Estimation

##### Conference

CVPR 2013

##### Association

KTH



#### VNect: Real-time 3D Human Pose Estimation with a Single RGB Camera

3d, two-stage, rgb, monocular, single-person, Mehta et al. 2017

##### Conference

SIGGRAPH 2017

##### Assocation

MPI-Inf

##### Website

http://gvv.mpi-inf.mpg.de/projects/VNect/

##### Note

[./note/vnect_siggraph_2017.md](./note/vnect_siggraph_2017.md)

realtime

Our method combines a new convolutional neural network (CNN) based pose regressor with kinematic skeleton fitting. Our novel fully convolutional pose formulation regresses 2D and 3D joint positions jointly in real time and does not require tightly cropped input frames. A real-time kinematic skeleton fitting method uses the CNN output to yield temporally stable 3D global pose reconstructions on the basis of a coherent kinematic skeleton.

#### Lifting from the Deep: Convolutional 3D Pose Estimation from a Single Image

3d, two-stage, 2d->3d, RGB, human3.6m, Tome et al. 2017, CPM

##### Conference

CVPR 2017

##### Website

http://www0.cs.ucl.ac.uk/staff/D.Tome/papers/LiftingFromTheDeep.html

##### Code

https://github.com/DenisTome/Lifting-from-the-Deep-release

##### Note

We propose a unified formulation for the problem of 3D human pose estimation from a single raw RGB image that reasons jointly about 2D joint estimation and 3D pose reconstruction to improve both tasks. We take an integrated approach that fuses probabilistic knowledge of 3D human pose with a multi-stage CNN architecture and uses the knowledge of plausible 3D landmark locations to refine the search for better 2D locations. The entire process is trained end-to-end, is extremely efficient and obtains state- of-the-art results on Human3.6M outperforming previous approaches both on 2D and 3D errors.

3d to 2d, 


#### 3D Human Pose Estimation = 2D Pose Estimation + Matching

two-stage, 3d, monocular, RGB, Chen et al. 2017

##### Conference

cvpr 2017

##### Association 

CMU

##### Code

https://github.com/flyawaychase/3DHumanPose

##### Note

we explore a simple architecture that reasons through intermediate 2D pose predictions. Our approach is based on two key observations (1) Deep neural nets have revolutionized 2D pose estimation, producing accurate 2D predictions even for poses with self occlusions. (2) Big-data sets of 3D mocap data are now readily available, making it tempting to lift predicted 2D poses to 3D through simple memorization (e.g., nearest neighbors). 
The resulting architecture is trivial to implement with off-the-shelf 2D pose estimation systems and 3D mocap libraries. Importantly, we demonstrate that such methods outperform almost all state-of-the-art 3D pose estimation systems, most of which directly try to regress 3D pose from 2D measurements.


#### DRPose3D: Depth Ranking in 3D Human Pose Estimation

3d. single-person, monocular, human3.6m, Wang et al. 2018

##### Conference

IJCAI 2018

##### Note

two-step approach;
depth ranking & 2d keypoint 2 subnet + a net to learn from both depth ranking & 2d pose;

good: 
solve the ill-pose problem of 2d->3d (multi 3d to 1 2d); 
depth ranking as classification problem to learn via dnn;

depth info via depth ranking, a new second step network



#### 3D Human Pose Estimation in the Wild by Adversarial Learning

3d, in the wild, RGB, gan, yang et al. 2018

##### Conference

CVPR 2018

##### Association

CUHK

##### Note

we propose an adversarial learning framework, which distills the 3D human pose structures learned from the fully annotated dataset to in-the-wild images with only 2D pose annotations. Instead of defining hard-coded rules to constrain the pose estimation results, we design a novel multi-source discriminator to distinguish the predicted 3D poses from the ground-truth, which helps to enforce the pose estimator to generate anthropometrically valid poses even with images in the wild. We also observe that a carefully designed information source for the discriminator is essential to boost the performance. Thus, we design a geometric descriptor, which computes the pairwise relative locations and distances between body joints, as a new information source for the discriminator. The efficacy of our adversarial learning framework with the new geometric descriptor has been demonstrated through extensive experiments on widely used public benchmarks. Our approach significantly improves the performance compared with previous state-of-the-art approaches.


#### Structured Prediction of 3D Human Pose with Deep Neural Networks

3d, RGB, monocular, Tekin et al. 2016

##### Conference

BMVC 2016 (oral)



#### Neural Body Fitting: Unifying Deep Learning and Model-Based Human Pose and Shape Estimation

3d, monocular

##### Conference

3dv 2018

##### Code 

https://github.com/mohomran/neural_body_fitting

##### Note

Direct prediction of 3D body pose and shape remains a challenge even for highly parameterized deep learning models. Mapping from the 2D image space to the prediction space is difficult: perspective ambiguities make the loss function noisy and training data is scarce. In this paper, we propose a novel approach (Neural Body Fitting (NBF)). It integrates a statistical body model within a CNN, leveraging reliable bottom-up semantic body part segmentation and robust top-down body model constraints. NBF is fully differentiable and can be trained using 2D and 3D annotations. In detailed experiments, we analyze how the components of our model affect performance, especially the use of part segmentations as an explicit intermediate representation, and present a robust, efficiently trainable framework for 3D human pose estimation from 2D images with competitive results on standard benchmarks. Code will be made available at this http URL



#### Learning camera viewpoint using CNN to improve 3D body pose estimation

RGB, 3d, monocular

##### Conference

3DV 2016




#### It’s all Relative: Monocular 3D Human Pose Estimation from Weakly Supervised Data

##### Conference

BMVC 2018



#### Single-Shot Multi-Person 3D Pose Estimation From Monocular RGB

RGB, 3d, multi-person, Mehta et al. 2018

##### Conference

3DV 2018

##### Association

MPI-Inf

##### Note

We propose a new single-shot method for multi-person 3D pose estimation in general scenes from a monocular RGB camera. Our approach uses novel occlusion-robust pose-maps (ORPM) which enable full body pose inference even under strong partial occlusions by other people and objects in the scene. ORPM outputs a fixed number of maps which encode the 3D joint locations of all people in the scene. Body part associations allow us to infer 3D pose for an arbitrary number of people without explicit bounding box prediction. To train our approach we introduce MuCo-3DHP, the first large scale training data set showing real images of sophisticated multi-person interactions and occlusions. We synthesize a large corpus of multi-person images by compositing images of individual people (with ground truth from mutli-view performance capture). We evaluate our method on our new challenging 3D annotated multi-person test set MuPoTs-3D where we achieve state-of-the-art performance. To further stimulate research in multi-person 3D pose estimation, we will make our new datasets, and associated code publicly available for research purposes.



#### Single Image 3D Interpreter Network

2d->3d, 3d, RGB, two-stage, monocular

##### Conference

ECCV 2016

##### Website

http://3dinterpreter.csail.mit.edu/

##### Code

https://github.com/jiajunwu/3dinn

##### Note

In this work, we propose 3D INterpreter Networks (3D-INN), an end-to-end framework that sequentially estimates 2D keypoint heatmaps and 3D object structure, trained on both real 2D-annotated images and synthetic 3D data. This is made possible mainly by two technical innovations. First, we propose a Projection Layer, which projects estimated 3D structure to 2D space, so that 3D-INN can be trained to predict 3D structural parameters supervised by 2D annotations on real images. Second, heatmaps of keypoints serve as an intermediate representation connecting real and synthetic data, enabling 3D-INN to benefit from the variation and abundance of synthetic 3D objects, without suffering from the difference between the statistics of real and synthesized images due to imperfect rendering. The network achieves state-of-the-art performance on both 2D keypoint estimation and 3D structure recovery. We also show that the recovered 3D information can be used in other vision applications, such as 3D rendering and image retrieval.



#### 3D Human Pose Estimation Using Convolutional Neural Networks with 2D Pose Information

3d, RGB, one-stage, regression, monocular

##### Conference

ECCV 2016 (Workshop)



#### Marker-less 3D Human Motion Capture with Monocular Image Sequence and Height-Maps

3d, video, monocular, Du et al. 2016

##### Conference

ECCV 2016

##### Association

ZJU

##### Website

http://zju-capg.org/heightmap/ (code, poster and demo provided)

##### Note

we introduce the additional built in knowledge, namely height-map, into the algorithmic scheme of reconstructing the 3D pose/motion under a single-view calibrated camera. 
Our novel proposed framework consists of two major contributions. Firstly, the RGB image and its calculated height-map are combined to detect the landmarks of 2D joints with a dual-stream deep convolution network. Secondly, we formulate a new objective function to estimate 3D motion from the detected 2D joints in the monocular image sequence, which reinforces the temporal coherence constraints on both the camera and 3D poses. 

Experiments with HumanEva, Human3.6M, and MCAD dataset validate that our method outperforms the state-of-the-art algorithms on both 2D joints localization and 3D motion recovery. Moreover, the evaluation results on HumanEva indicates that the performance of our proposed single-view approach is comparable to that of the multiview deep learning counterpart.



#### Recovering 3D Human Pose from Monocular Images

3d, RGB, monocular

##### Conference

T-PAMI 2005



#### Pose-Conditioned Joint Angle Limits for 3D Human Pose Reconstruction

3d, Akhter et al. 2015

##### Conference 

CVPR 2015

##### Code

bayesian network




#### Efficient ConvNet-based Marker-less Motion Capture in General Scenes with a Low Number of Cameras

3d, multi-view, video

##### Conference

CVPR 2015



#### 3D Convolutional Neural Networks for Human Action Recognition

##### Conference

TPAMI 2012



#### 3D human pose estimation in video with temporal convolutions and semi-supervised training

3d, video

##### Conference

arXiv 2018

##### Code

https://github.com/facebookresearch/VideoPose3D, official, pytorch, code and models available

##### Website

https://dariopavllo.github.io/VideoPose3D

##### Note

In this work, we demonstrate that 3D poses in video can be effectively estimated with a fully convolutional model based on dilated temporal convolutions over 2D keypoints. We also introduce back-projection, a simple and effective semi-supervised training method that leverages unlabeled video data. We start with predicted 2D keypoints for unlabeled video, then estimate 3D poses and finally back-project to the input 2D keypoints. In the supervised setting, our fully-convolutional model outperforms the previous best result from the literature by 6 mm mean per-joint position error on Human3.6M, corresponding to an error reduction of 11%, and the model also shows significant improvements on HumanEva-I. Moreover, experiments with back-projection show that it comfortably outperforms previous state-of-the-art results in semi-supervised settings where labeled data is scarce. 


#### 2D/3D Pose Estimation and Action Recognition using Multitask Deep Learning

RGB, 2d, 3d, action recognition

##### Conference 

CVPR 2018



<!-- 3d video -->


#### 3D Shape Estimation from 2D Landmarks: A Convex Relaxation Approach.

3d, video, two-stage

##### Conference 

CVPR 2015 (oral)

##### Association

UPenn

##### Code

https://fling.seas.upenn.edu/~xiaowz/dynamic/wordpress/shapeconvex/

##### Website

https://fling.seas.upenn.edu/~xiaowz/dynamic/wordpress/shapeconvex/



#### Sparseness meets deepness: 3D human pose estimation from monocular video

3d, two-stage, video

##### Conference

CVPR 2016

##### Association

UPenn

##### Website

https://fling.seas.upenn.edu/~xiaowz/dynamic/wordpress/monocap/

##### Code

https://fling.seas.upenn.edu/~xiaowz/dynamic/wordpress/monocap/

##### Note

[./note/sparseness_meets_deepness_cvpr_2016.md](./note/sparseness_meets_deepness_cvpr_2016.md)











### Surveys

---

#### A survey of human pose estimation: The body parts parsing based methods

##### Conference

JVCIR 2015

##### Note

survey on human pose estimation, vision-based, body-parsing methods.

including 2d (single & multi, image & video), 3d (single, image)


#### Key Developments in Human Pose Estimation for Kinect

##### Note

depth image (kinect)


#### Vision-based human motion analysis: An overview

##### Conference

CVIU 2007



#### A Survey of Advances in Vision-Based Human Motion Capture and Analysis

##### Conference

CVIU 2006


#### Advances in view-invariant human motion analysis: a review

2010


#### Human Pose Estimation and Activity Recognition From Multi-View Videos: Comparative Explorations of Recent Developments

##### Conference

JSTSP 2012


#### Human motion analysis: a review

##### Conference

CVIU 1999


#### The visual analysis of human movement: a survey

##### Conference

CVIU 1999


#### Monocular model-based 3D tracking of rigid objects: a survey

##### Conference

CGV 2005




#### 3D Human pose estimation: A review of the literature and analysis of covariates

cviu 2016

a survey on 3d human pose estimation, some deep learning covered









### Datasets

---


#### Microsoft COCO: Common Objects in Context

##### Conference

ECCV 2014



#### PoseTrack: Joint Multi-Person Pose Estimation and Tracking

2d, multi-person, tracking, video, PoseTrack dataset

##### Conference

CVPR 2017

##### Code

https://github.com/iqbalu/PoseTrack-CVPR2017

##### Webiste

http://pages.iai.uni-bonn.de/iqbal_umar/PoseTrack/



#### HUMANEVA: Synchronized Video and Motion Capture Dataset and Baseline Algorithm for Evaluation of Articulated Human Motion

humaneva Dataset, 3d, single-person

##### Conference

IJCV 2010



#### Human3.6M: Large Scale Datasets and Predictive Methods for 3D Human Sensing in Natural Environments

human3.6m Dataset, video, single-person, 3d, multi-view

##### Conference 

T-PAMI 2013

##### Note

Dataset provided by Martinez et al. ICCV 2017: https://www.dropbox.com/s/e35qv3n6zlkouki/h36m.zip


#### Benchmarking and Error Diagnosis in Multi-Instance Pose Estimation

##### Conference

ICCV 2017

##### Code

https://github.com/matteorr/coco-analyze

##### Website

http://www.vision.caltech.edu/~mronchi/projects/PoseErrorDiagnosis/



#### 2D Human Pose Estimation: New Benchmark and State of the Art Analysis

MPII Dataset, 2d, single-person and multi-person

##### Conference

CVPR 2014



#### Clustered Pose and Nonlinear Appearance Models for Human Pose Estimation

2d, RGB, single-person, LSP Dataset

##### Conference

BMVC 2010
