# 人体姿态估计：综述



## 摘要



## 一. 介绍

（基本概念）

给定一幅图像或一段视频，人体姿态识别（Human Pose Estimation）就是去恢复其中人体的二维或者三维关节点位置的过程。传统的人体姿态识别方法使用额外的硬件去捕捉人体姿态，并根据捕捉到的关节点构建人体骨架，但这些方法往往效率较低或者成本过高。在过去十年里，许多研究使用计算机视觉领域的方法对人体姿态进行识别。在本篇综述中，我们主要讨论基于计算机视觉的人体姿态识别。

（应用）

人体姿态识别可以应用于广泛的科研和工程领域，其中包括：

人机交互：

视频监控：

行为识别（action recognition）[8]

行人再识别（person re-id）



数字娱乐：



医疗成像：

运动场景：

......



（输入类型）

根据输入模态的不同，人体姿态识别算法可以分为两类：基于深度图（RGB-D)的算法，以及直接基于RGB图像的算法。

基于RGB图像（和视频）的人体姿态识别，在早期主要是以获取人体关节的二维坐标作为目标；近年来，随着深度学习的发展，出现了越来越多3D人体姿态识别的研究。基于RGB图像的姿态识别，只需要普通的摄像头，因此数据采集更为容易，应用面也更广，是当前的研究热点，目前在学术上已经取得了很好的结果，即使是针对比较复杂的场景。不过，真正能够实际应用的3D关键点检测方法并不多，对计算资源的要求也比较高。

基于深度图的人体姿态识别对于图像采集设备要求过高，因而存在应用易受限的问题。由于深度图很容易将前景和背景分离，该问题更为简单。这一方面的研究在深度学习兴起之前也已经达到了比较理想的效果。经典方法以随机森林作为基础。基于深度图的姿态识别，主要问题在于目前数据集较少且规模较小，同时，深度摄像头适用场景更加局限，往往仅限于室内；同时，深度成像的成本更高。目前手势识别（Hand Pose Estimation）方面利用深度图做了不少研究。

在人体姿态识别领域，早期的方法主要使用特征提取子去提取局部的特征，检测出关节后，再用复杂的结构化图模型去推断整体的姿态。从2014年（DeepPose）开始，卷积神经网络被应用到人体姿态识别中，并取得了很好的效果，目前卷积神经网络在人体姿态识别，尤其是基于RGB图像的人体姿态识别中已经成为主流。



（方法：regression-based, detection-based）



（目标关键点维度：二维，三维）



（基本问题）

1. 遮挡和变形等问题。遮挡对人体姿态识别一直是一个很大的问题。2016年CMU的CVPR论文（convolutional pose machines）在遮挡问题方面表现出了较好的鲁棒性。此外，在多人场景的人体姿态识别中，不同的人肢体的交互和重叠等情况也大大增加了难度。
2. 一些在训练数据中很少见，比较特别的姿态
3. 外观的变化，如不同服饰和光照条件等变化带来的影响
4. 性能（performance）和效率仍然存在问题。目前的方法只能做到在titanx这样的设备上达到准实时的效果（15fps）。



重难点

1. 整体信息

对关键点检测重要的不仅是局部（local）信息（如关节点周围）。单个关节点周围的信息往往不足以正确的检测和判定，所以整体（global）的信息，也就是spatial context也是很重要的，需要有能够提取多种尺寸范围，多种清晰度的特征的方法。

对此问题：

DeepPose（2014）：多阶段，初始阶段提取整体信息回归出坐标，后续阶段用坐标周围的子图来提取局部信息refine结果。



2. 人体结构

在人体姿态识别中，一些变化较小，易于检测的关节（如头部，脖子）的位置对于其他关节的检测也有很大的影响，比如不同关节点之间的位置关系需要符合人体的实际情况。因此，需要用显示（如graphical model）或隐式的方法来模拟人体结构信息。比如ICCV2017的论文compositional human pose regression的论文，提出了一个组合损失函数。

3. 肢体信息

一些论文不局限于关节点的检测，还试图去寻找和利用关节点之间的连接，也就是骨骼（bone）或者肢（limb）的信息。如CVPR2017, CMU的论文（PAF, aka OpenPose），用part affinity fields去表现关节点之间的连接，同时生成关节和连接两者的热图（heatmap），然后将关节和连接的信息转换为图模型；

（这三点感觉有点重叠，会重新归纳）



## 二. 相关调查和综述

2015, zju, 对于基于视觉的人体姿态估计做了调查[12]。当时占据主流的方法还是handcrafted feature + Pictorial Structure一类的，对深度学习的方法只有简单涉及（[7], [13]）。在这三四年来，深度学习的方法超越了这些传统方法，在新的数据集上state-of-the-art的标准高了很多，因此新的调查是很有必要的。



3D Human pose estimation: A review of the literature and analysis of covariates (2016, 3d human pose estimation, cvig)*



## 三. 图像中的二维单人姿态估计

当前面临的主要问题

1. 复杂的背景
2. 光照条件的变化
3. 各种各样的姿态
4. 人的尺度（大小）和拍摄角度不一



主流方法归纳

传统方法：基于pictorial structures，DPM

在深度学习攻占人体姿态识别领域前，主要的方法是handcrafted特征提取子和关节点模型的结合使用。对于特征提取，最常用的是HOG和shape context。

*（接下来的部分需要写下效果的比较和不足，包括performance/efficiency等，后面补）*



自2014年DeepPose[7]开始，基于深度学习的方法得到越来越广泛的使用，并且逐渐成为主流。目前对于2D的RGB单人姿态识别，最好的方法都是使用了深度学习的。

基于深度学习的2D人体姿态估计，方法主要分为两类，一类是直接回归坐标（regression-based），另一类是生成热力图来检测出关节点坐标（detection-based）。直接回归坐标的方法是2014年DeepPose[7]首先提出的，由于CNN在图片分类上效果很好，所以想到了直接用AlexNet提取图片特征后，回归出关节点坐标。为了解决将图片整体用于回归导致细节信息丢失和准确率较低的问题，DeepPose采用了级联回归模型，用多个回归器（regressor）对上一阶段输出的坐标进一步改进，每个阶段用CNN提取前一阶段检测到的关键点周围的局部图（sub-image）的信息，重新回归。2016年IEF[4]的方法，为了能够充分利用输出空间（output space）的结构信息，设计了一个迭代错误反馈（Iterative Error Feedback）模型，并不是直接回归，而是利用每个阶段的错误反馈来改善上一阶段的回归结果。

用热力图来回归坐标的方法以CPM[5]和stacked hourglass[6]为代表。之前的方法没有解决的一个问题是不能很好地检测多种尺度下的人体。所以需要考虑让网络克服这一问题，并且学习到关节与关节之间的关系(pair wise relation)。2014年，Tompson等人提出了CNN+图模型的方法，用金字塔形的卷积神经网络提取特征，检测出可能的关键点，并用图模型来处理关节点之间的关系。这里的关系建模是各个关节点两两之间连接成的图模型，wei yang等人[10]想到了用树状模型来代替图模型，对人体所有关节形成的树状结构进行建模。

图模型的一大问题就是计算效率太低，所以一个改进的方向就是不使用图模型，而是隐式地去学习人体各个关节点之间的关系。为了达到这个目标，需要让网络能够提取不同尺度范围的信息，并充分利用空间信息。2016年shih-en wei等人提出了CPM[5]的方法，使用卷积姿态机，大卷积核提升感受野，多阶段回归。CPM的方法把姿态机和卷积神经网络结合，设计了一个多阶段的网络结构，不同阶段神经网络的感受野大小不同，因此能够提取不同范围（从局部到整体）的信息，同时，前一阶段的belief map和图片特征会作为下一阶段的输入，因此达到了将不同尺度的信息混合处理的效果。同时，针对神经网络层次过深导致梯度消失的问题，CPM提出了中间层监督（intermediate supervision）的方法来解决。（表现，效果，着重强调。。。）

虽然CPM效果很好，但是大卷积核的堆叠导致了计算资源要求较高，所以需要有一种新的架构，能够降低计算量，同时提升感受野。2016年ECCV的堆叠沙漏[6]方法同样采用了多阶段结构，但借鉴了FCN[11]，每个阶段的网络都是类似FCN的沙漏型，一方面，计算量减小，另一方面，保持了感受野。（提一下单个阶段的网络结构，和FCN的区别）

（接下来2017年主要是针对stacked hourglass改结构的方法，）

2017年也出现在将GAN应用到HPE的方法（posenet）[17]，不过其生成器（generator）和鉴别器（discriminator）仍然是基于stacked hourglass的网络结构，可以看作是hourglass的变体。



（补一个分点：利用人体结构和关节点相对位置信息的方法，以CUHK 2016，2017为代表）

regression-based:

DeepPose (cvpr 2014)*

dual-source cvpr 2015



detection-based, explicitly model dependencies:

Tompson et al. nips 2014

object localization 2015 (to read)

li et al. 2014 (heterougeneous multi-task learning)

end-to-end cuhk 2016

structured feature learning cuhk 2016

Learning feature pyramids for human pose estimation (iccv 2017)

multi-attention cuhk 2017 (to write)



implicitly model dependencies (through iteration)

cpm (cvpr 2016) *

stacked hourglass （eccv 2016）*

human pose estimation with iterative error feedback (2016) *



combine regression and detection:

Human pose estimation via convolutional part heatmap regression (eccv 2016)

regression-detection corr 2017

integral regression eccv 2018



not certain:

nips 2014 pairwise relation

An Efficient Convolutional Network for Human Pose Estimation (bmvc 2016) 

多尺度特征的实现思路，一个是输入图片大小不同（如tompson 2014, rafl bmvc 2016),通过deconv把小的feature map变大;另一个是skip layer，神经网络本身来实现。见bmvc2016的图，实现了这两种。


## 四. 图像中的二维多人姿态估计

多人姿态识别，相对单人姿态识别来说，多了以下几个问题：

1. 图像中人数不确定。（这也是基于回归的人体姿态识别方法表现相对较差的原因之一——需要预先知道人数，不过如果使用自顶向下的方案，先检测人的存在，再对每个单人的区域进行单人人体姿态回归找到关节，那么这个问题就解决了，但又会带来耗时线性增长等问题）
2. 人体之间的交互和重叠，比如一个人身体一部分被另一个人遮挡，那么就很容易出现关节点匹配到错误的人身上的问题。

多人姿态识别的方法有以下两种思路：

### 4.1 自顶向下

自顶向下（top-down）：也叫two-step framework。分为两步，先进行行人检测，得到边界框，再对每个人分别使用单人姿态识别方法，在每一个边界框中检测人体关键点，连接成一个人形。代表：mask-rcnn， RMPE。有以下优势：

1. 可以直接利用现有的单人姿态识别方法或者基于它们进行改进。比如yolo+cpm (cvpr 2016 cmu)就是现在的一个常用于比较的baseline。

有以下缺点：

1. 受检测框的影响太大，漏检，误检，IOU大小等都会对结果有影响。也就是说，一旦人体检测错了，后面的单人关键点检测步骤无法改善结果（这个问题也叫early commitment）。
2. 因为对图像中所有检测到的单人要逐一进行关节检测，因此第二阶段耗时随着图像中人数线性变化。

#### 人体检测

（这部分不重要，可能要删去）

主要是用了目标检测（object detection）的方法，比如faster-rcnn，ssd。但是人体检测部分出错对结果也会有比较大的影响，所以这方面也有一些工作在进行。

在cvpr2017 g-rmi [18]中，使用faster-rcnn来对图像中多个人体进行检测，并对检测结果框（bounding-box）进行image crop。

iccv2017的RMPE方法，使用SSD-512检测人体。RMPE方法致力于解决前面提到的early commitment的问题，也就是改善人体检测框的质量。它通过空间变换网络，对同一个人体所产生的不同的裁剪区域（检测框）都变换到使人体在中央，这样就不会对一个人体的不同候选框检测出不同的关键点。



#### 关键点检测

在g-rmi中，同时采用热力图和坐标偏移，构建了一种新的ground truth。对于检测出的单人bounding-box，采用fully convolutional resnet对人体同时预测热力图和坐标偏移，最后将热力图和坐标偏移融合得到精确的关键点定位。

cvpr2018的cpn论文，针对不同类别关键点检测难度不同的问题，使用多阶段网络CPN，先用globalNet检测较简单的关键点，再用RefineNet检测难检测的/被遮挡的/不可见的关键点。



### 4.2 自底向上

自底向上（bottom-up）：也叫part-based framework。检测出所有关节点，再判断它们分别属于哪一个人，拼接成一个个人体骨架（skeleton）。容易出现将属于不同人的关节拼接成一个人的情况。代表：PAF，DeepCut，DeeperCut。

有以下优势：

1. 图像中人数和耗时无关
2. 到最后才分出不同的人，因此不存在early commitment的问题。



有以下缺点：

1. 大部分是提取局部特征+利用全局信息推断出人体骨架的方法，没有直接提取全局范围的特征。在body-parsing的阶段非常耗时。

#### 关键点检测

（相对不重要，可能删去）

part-detector

DeepCut用了fast-rcnn的方法。

DeeperCut使用了ResNet对DeepCut的关键点检测子进行改进，效果更好，速度更快。

#### 关键点聚类

对所有人体的关键点进行聚类，将不同人的关键点分开，同一个人的关键点连接起来，从而产生各个人体的姿态。

DeepCut将所有候选关键点（未知属于的人体和类型）连接成一个全连接图，然后进行整数线性规划(integer linear programming)。这是一个NP困难问题，因此耗时很久（一张图处理时间要几个小时）。

DeeperCut在其上面进行改进，使用image-dependent pairwise scores来大大提高了DeepCut的速度，但还是太慢（单张图时间分钟为单位）。

PAF的工作，为了解决单纯使用中间点会造成肢干（limb）连接错误的问题，不仅检测关节，还用并行的网络检测肢干，使用了向量场来对肢体结构进行建模，这样就给关键点加上了方向信息，有效解决了错连问题。（如何做到想对于DeeperCut大大加速的？这一点后面写）



deepCut (cvpr 2016) to write

deeperCut (eccv 2016) to write

associative embeddings nips 2016

openpose/paf (cmu) (cvpr 2017) to write

mask-rcnn (ms) (2017) to write

g-rmi (google) (2017) to read

Cascaded Pyramid Network for Multi-Person Pose Estimation (cvpr 2018) to read



densepose (2018)

RMPE (2017)*



## 五. 视频中的二维单人姿态估计 

视频中的姿态估计，其数据不再是单幅图像，而是连续的多张图像。这意味着人体姿态存在时间上的连续性，也就是说，前后帧的动作往往差别不大，具有很强的相关性，这个相关性的信息可以用于帮助判断当前人体的位置和姿态。总的来说，在视频中，上下文信息（context information）不仅包括空间上的信息（其他身体关节，其他人动作的交互），还包括了时间上的信息（前后动作的一致性和细微变化）。



Flowing convnets for human pose estimation in videos （ICCV 2015）

lstm pose machine

Personalizing Human Video Pose Estimation (CVPR 2016)

sparseness meets deepness (3d), 2016

detect and track cvpr 2018

simple baselines eccv 2018



## 六. 三维单人姿态估计

from image:

deep kinematic

Coarse-to-Fine Volumetric Prediction for Single-Image 3D Human Pose (CVPR 2017)

weakly-supervised 3d iccv 2017





from video:

Sparseness meets deepness: 3D human pose estimation from monocular video (CVPR 2016)

Direct Prediction of 3D Body Poses from Motion Compensated Sequences



problem:

convnet-based techniques, the lack of training data. 

## 七. 数据集和评价指标

### 7.1 评价指标

#### 7.1.1 2d

PCK



PCP (for 2d and 3d)

Percentage of Correctly estimated Parts 



#### 7.1.2 3d

##### MJAE

##### 3D Error ($\epsilon$)



### 7.2 常用数据集



#### 7.2.1 2D单人图像数据集

 **MPII：**[http://human-pose.mpi-inf.mpg.de/](http://human-pose.mpi-inf.mpg.de/)

- 样本数：25K
- 关节点个数：16
- 全身，单人/多人，40K人

**FLIC：**[https://bensapp.github.io/flic-dataset.html](https://bensapp.github.io/flic-dataset.html)

- 样本数：2W
- 关节点个数：9
- 全身，单人

**LSP：**[http://sam.johnson.io/research/lsp.html](http://sam.johnson.io/research/lsp.html)

- 样本数：2K
- 关节点个数：14
- 全身，单人

 

#### 7.2.2 2D多人图像数据集

**MSCOCO**：[http://cocodataset.org/#download](http://cocodataset.org/#download)

- 样本数：>= 30W
- 关节点个数：18
- 全身，多人，10W人。



**AI Challenge**：[https://challenger.ai/competition/keypoint/subject](https://challenger.ai/competition/keypoint/subject)

- 样本数：21W Training, 3W Validation, 3W Testing
- 关节点个数：14
- 全身，多人，38W人。



#### 7.2.3 3D单人数据集

**Human 3.6m**：[http://vision.imar.ro/human3.6m/description.php](http://vision.imar.ro/human3.6m/description.php)

- 样本数：360W 图片
- 视频数：1376
- 关节点个数：24
- 摄像头个数：4
- 全身，单人
- 主体数：11



humaneva i & ii

- 视频数：56
- 主体数：4



densepose (3d, 多人)



## 八. 当前进展和趋势

单人姿态识别，当前以CNN为主，stacked hourglass [6]是非常具有代表性的一类方法。目前在MPII数据集上效果最好的几种方法[14, 15]都是基于堆叠沙漏的方法（并且已经接近到顶）。

多人姿态识别，Zhe Cao et al. cvpr 2017 (PAF) [1]。

cmu的openpose，结合了CPM（单人），PAF（多人)， Hand Keypoint Detection in Single Images using Multiview Bootstrapping （cvpr 2017）[16]。

mpi-inf vnect real time 3d siggraph 



主要困难仍然是遮挡（occlusion）问题。同时，由于目前的算法复杂度比较高，离实际应用还有一定的距离要走。

趋势：2d到3d [15]; human pose tracking; 

## 九. 总结





*[Note]*

*: 已读



*<-- last updated 2018/7/1 -->*



