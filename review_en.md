# Deep Human Pose Estimation: A Survey

## Abstract

## 1. Introduction

Human pose estimation (HPE) is the process of inferring the 2d or 3d configuration of the human body in a given image or a sequence of images. As a important task in computer vision, it has been used in a broad range of scientific and research domains, like to analyze human behaviors in smart surveillance systems, to control avatar motion in realistic animations, to analyze gait pathology in medical practices, to study the movements of athletes in sports competitions, and to interact with computers.

Traditionally, a human body pose can be accurately reconstructed from the motion captured with optical markers attached to body parts. These marker-based systems usually use multiple cameras to capture motions simultaneously. However, they are not suitable for real-life non-invasive applications, and the equipment is quite expensive, confining their applications to limited scenarios. 

During the past decade, considerable research efforts have been devoted to human pose estimation in computer vision domain, and focused on markerless methods. Several types of images can be used as input: RGB or grayscale images, depth images, infrared images, and others. Infrared images capture infrared light, and are extremely useful for night vision, but they are not in the scope of this review. Depth images contain information regarding the distance of objects in the image to the cameras, and require specialized equipment. Such equipment is much less expensive compared with those for acquiring motion capture data. Commercial products include Microsoft Kinect, the Leap Motion, and GestureTek, which provide APIs to acquire depth data. Human pose estimation has seen the most success when utilizing depth images in conjunction with color images: real-time estimation of 3D body joints and pixelwise body part labelling have been possible based on randomized decision forests. Estimation accuracy from depth images are comparatively more accurate, but these devices can only acquire images within certain distance limit (around eight meters). Besides, there are only a few datasets of depth images, with relatively small scale.

RGB images are the most common input for computer vision tasks, and take up a vast majority of pictures on the web. RGB images or videos can be captured with oridinary cameras, making the data collection much easier and the application domain broader. Currently human pose estimation based on RGB input has achieved great result in research, and but there still exist problems to solve to apply it in real life, especially for 3d estimation. 

Early methods for RGB-based human pose estimation usually use feature extractors to extract local features of body parts and then, as seen most common, probabilistic graph models to infer the global body poses. As deep learning became popular, the performance of RGB-based human pose estimation methods has also been boosted, like many other tasks in computer vision. Starting from DeepPose (Toshev et al. 2014), Convolutional Neural Networks has gradually taken up the position as the most used methods for human pose estimation.

Human pose detection from a single image is a severely under-constrained problem, due to the intrinsic one-to-many mapping nature of this problem. One pose produces various pieces of image evidence when projected from changing viewpoints. This problem has been studied a lot, but is still far from being completely solved. Effective solutions for this problem need to tackle illumination changes, shading problems, and viewpoint variations. Furthermore, human pose estimation problems have specific characteristics. First, the human body has high degrees of freedom, leading to a high-dimensional solution space; second, the complex structure and flexibility of human body parts causes partially occluded human poses which are extremely hard to recognize. Besides, for 3d human pose estimation, depth loss resulting from 3D pose projections to 2D image planes makes the estimation extremely difficult.

Although having investigated the issues of human body part configuration, human body detection and human motion in the previous studies, there still lacks a survey to summarize the most recent progress on body pose estimation. In this survey, we mainly review the recent advances in vision-based human pose estimation. As it is hard to cover all these fields within a single survey, we mainly focus on deep learning based methods. Basic network architectures are briefly reviewed and existing datasets and evaluation metrics are introduced, with future trends discussed.

<!-- 
Human pose estimation techniques become more and more mature in the past decades. Being the great interest of different domains, new applications constantly emerge along with the technological evolutions. Human pose estimation is not only an important computer vision problem, but also plays critical role in a variety of real-world applications in the following.

Video Surveillance. Video surveillance aims at tracking and monitoring the locations and motions of pedestrians in special circumstances. It is the earliest application area that HPE technologies have been used. The common scenes are the supermarket and airport passageway.

Human�Computer Interaction (HCI). Advanced human computer interaction systems with human pose estimation have been developed rapidly. In these systems, instructions can be analyzed accurately by capturing the human body poses. In recent years, intelligence driving emerges as a novel practical application.

Digital Entertainment. Digital entertainment, including computer games, computer animation and films, has become a huge industry and an active domain in recent years. For instance, People enjoys the pleasure the body sensor games give to them. Also, In the pre-production of the special effects for movie Avatar [2], actors wear the special equipments to animate the activities of Avatars.

Medical Imaging. Human pose estimation has been widely used in the automatic medical field. A specific instance is that HPE can be used to assist doctors to check patients� activities from the remote monitor, which greatly simplifies the therapeutic process.

Sports Scenes. In sports news and live broadcast, human pose estimation is employed to track athletes� locations and activities. Moreover, the estimated poses can be used to employed the detailed movements of their actions.

Other applications include military, children mental development, virtual reality, and so on. 

In recent years, various devices and commercial systems have been released accompany with HPE technology, including Microsoft Kinect sensor [3,4], Leap Motion [5], body mounted camera [6], 3D laser scanner and infrared light source [8]. These commercial systems have quite different implementation principles and application fields.
 -->

### 1.1 Related Surveys and Other Resources

The reader can found several surveys of human pose estimation in the literature. The early works of Aggarwal et al. 1999 and Gavrila et al. 1999 ofter an overview of the initial methods in this field. Moeslund et al. 2006 and Poppe et al. 2007 cover the whole vision-based human motion capture domain, but are also outdated. The most recent comprehensive surveys are Liu et al. 2015, which studies human pose estimation for both 2d and 3d problem based on image or video input under single-view or multi-view settings, and Gong et al. 2017, which investigate pose estimation based on monocular input, as well as Sarafianos et al. 2016, which is limited to 3d pose estimation problems. 

Some other existing reviews focus on more specific tasks. For instance, a review on view-invariant pose representation and estimation is offered by Ji and Liu (2010). In the work of Sminchisescu (2008) , an overview of the problem of reconstructing 3D human motion from monocular image sequences is provided, whereas Holte et al. (2012) present a 3D human pose estimation review, which covers only model-based methods in multi-view settings. The survey from Lepetit et al. 2005 studies model-based approaches, which employs human body knowledge such as the human body�s appearance and structure for the enhancement of human pose estimation. 

However, deep learning based methods that achieves significant progress in human pose estimation and attracts a lot of research attention, are not well introduced in these previous surveys. We feel their is a need to investigate deep learning based methodologies. It is encouraged for the reader to go through the work of Liu et al. 2015, which our work is complementary to. 

### 1.2 Scope and Taxonomy of the Survey

As it is hard to cover all these fields within a single survey, the primary goal of our survey is to summarize the recent advances of human pose estimation based on deep learning. As previously surveys on human pose estimation either appear before the booming of deep learning or merely introduce a few related methods, we conduct a systematic, up-to-date research of approaches published in the recent 5 years. The scope of the survey includes both 2d pose estimation, which has seen a great development in recent years and is now considered a nearly-solved problems, and 3d human pose estimation, which is more realistic but remains largely to be solved. As human pose estimation is strongly related to fields like video surveillance and action recognition, where motion cues are important, we will cover pose estimation methods for both RGB image and video (image sequences). As multi-view setting is quite limited in the lab and not necessary due to the recent progresses, we choose to pay our attention on only monocular pose estimation.

In this survey, deep neural network architectures that are used frequently used in recent years are introduced in section 2. For better comparison between different methods, they are divided into 4 parts, including 2d single-person pose estimation in images (see section 3), 2d multi-person pose estimation in images (see section 4), 2d pose estimation in videos (see section 5) and 3d human pose estimation (see section 6). In section 7, various datasets are summarized and compared, along with their evaluation metrics. Finally, section 8 concludes this survey and foresee the future directions for HPE study.

<!-- (some gan network included) -->

## 2. Network Architecture

In deep-based human pose estimation, deep neural networks acts as feature extractors. In this survey the architectures are categorized as backbone and specific networks, as shown in Table *. Inspired by the extraordinary success on the ImageNet challenge, the typical CNN architectures, such as AlexNet, VGGNet, GoogleNet and ResNet, are introduced and widely used as the baseline model in human pose estimation (directly or slightly modified). In addition to these mainstream networks, there are some novel architectures designed for specific tasks in computer vision, namely the specific networks. These networks are adopted quite often in various methods for human pose estimation, and usually consist of multiple modified backbone networks. 

(table * here)

### 2.1 Backbone Networks

The commonly used network architectures of deep human pose estimation have always followed those of deep object classification and evolved from AlexNet to ResNet rapidly. In 2012, AlexNet was reported to achieve state-of-the-art recognition accuracy in the ImageNet large-scale visual recognition competition (ILSVRC) 2012, exceeding the previous best results by a large margin. AlexNet consists of five convolutional layers and three fully connected layers, and it also integrates various technique, such as rectified linear unit (ReLU), dropout, data augmentation, and so forth. ReLU was widely regarded as the most essential component for making deep learning possible. Then, in 2014, VGGNet proposed a standard network architecture that used very small 3x3 convolutional filters throughout and doubled the number of feature maps after the 2x2 pooling. It increased the depth of the network to 16-19 weight layers, which further enhanced the flexibility to learn progressive nonlinear mappings by deep architectures. In 2015, the 22-layer GoogleNet introduced an inception module with the concatenation of hybrid feature maps, as well as two additional intermediate softmax supervised signals. It performs several convolutions with different receptive fields (1x1, 3x3 and 5x5) in parallel, and it concatenates all feature maps to merge the multi-resolution information. In 2016, ResNet proposed to make layers learn a residual mapping with reference to the layer inputs F(x) := H(x) + x rather than directly learning a desired underlying mapping H(x) to ease the training of very deep networks (up to 152 layers). The original mapping is recast into F(x) + x and can be realized by shortcut connections. With the evolved architectures and advanced training techniques, such as batch normalization (BN), the network becomes deeper and the training becomes more controllable, and the performance of object classification is continually improving. We present these mainstream architectures in Fig. 8.

### 2.2 Specific Networks
<!-- 
r-cnn
rcnn 
fpn general purpose; compromises high resolution weak representations with low resolution strong representations. powerful localization and classification properties of FPN proved to be very successful in detection segmentation and keypoints tasks. (used in multiposenet, cpn)
fcn
cpm (paf, lstm pose machine)
stacked hourglass (yang 2017, chu 2017, cpn 2018, zhao 2017)
 -->
Some networks originally designed for various computer vision tasks have also being adopted for human pose estimation. Networks in R-CNN family are two-stage detectors for general object detection. R-CNN use selective search algorithm to generate object proposals and then CNN as classifier. Fast R-CNN solved some of the drawbacks of R-CNN by, instead of feeding the region proposals to the CNN, feeding the input image to the CNN to generate a convolutional feature map, from which the region of proposals are identified and sent to Fully Connected Layers for classification. Later, Faster R-CNN is proposed. Instead of using selective search algorithm on the feature map to identify the region proposals, a separate network is used to predict the region proposals, which are then reshaped using a RoI pooling layer and used for classification. 
The feature pyramid network or FPN, is a general-purpose network that can produce multi-scale features at relatively low cost. It compromises high-resolution weak representations with low-resolution strong representations. The powerful localization and classification properties of FPN proved to be very successful in detection, segmentation as well as keypoints localization tasks. 
The networks proposed by Wei et al. 2016 and Newell et al. 2016 has also proved to be useful for human pose estimation. As they are capable of learning multi-scale features through a iterative process and achieves good performance on benchmark datasets, many later works adopted them as basic network. The network proposed by Newell et al. 2016 consist of multiple hourglass networks stacked together, each use Residual Module as unit and performs downsampling and upsampling in a way similar to the FCN proposed by Long et al. 2015. 

## 3. 2d single-person pose estimation in images

2D single-person pose estimation in still images aims at locating all body parts of a person instance. It is often assumed in single-person pose estimation that there only exists one person instance that takes up the entire image patch, or the scale and location of the person bounding boxes are provided. It also often serves as a part of some methods for 2d multi-instance pose estimation or 3d pose estimation. 

### 3.1 Regression-Based Approach

With regard to deep learning based methods for human pose estimation, they can be roughly divided into regreesion-based and detection-based methods. The idea of regression-based methods, which is to directly mapping the input image to the 2D coordinates of body parts, was first proposed by Toshev et al. 2014, in the work called DeepPose. As CNN achieves great performance for image classification, the network structure of AlexNet was adapted and it took a whole image as input and output the 2d coordinates of body parts, which, compared with traditional part-based methods, utilized well the spatial information existing in the holistic view. To compensate the lost of detailed local information, a cascade of pose regressors is used, which takes the image patches around the predicted coordinates and refined the results using the local appearance information. Carreira et al. 2016 also used a multi-stage network, in which a Iterative Error Feedback model is able to correct the prediction of each stage using the error feedback. Fan et al. 2015, largely inspired by DeepPose, proposed to combine the holistic view and local appearance together as input using priori knowledge in the form of binary mask, and carried out a multi-task learning of joint localization and joint detection on object proposals. Considering the output joint coordinates are independent of each other in the direct fomulation fomulation, which fails to capture their relationships, a bone-based pose representation, instead of commonly joint-based ones, and a compositional loss function to incorporate their dependencies are proposed in Sun et al. 2017.

### 3.2 Detection-Based Approach

The methods of estimation-as-regression were limited in accuracy, probably because of inefficient direct regression of pose vectors from images, which is highly non-linear and difficult to learn. Some other methods generated heatmaps representing the pixel-wise likelyhood of a joint occurring in each spatial location using ConvNets from images, and detect keypoints from these heatmaps, typically choosing the maximum point as the predicted body part location. Such methods were often combined with explicit graphical models. Tompson et al. 2014 proposed a hybrid architecture consising of a deep Convolutional Neural Network and a Markov Random Field, which is able to exploit structural domain constraints such as geometric relationships between each body joint pairs. As a extension, a position refinement model was added to improved the low accuracy caused by pooling layers in convolutional neural networks. Similarly, Yang et al. 2016 propose to combine domain priori knowledge into ConvNets by learning part appearance terms with convolutional layers and spatial relationship terms with message passing layers, which is able to learn the relationships between multiple parts and is adaptable for both tree model and loopy structure model, compared with the pairwise fully connected graph in Tompson et al. 2014. Chu et al. 2016 proposed that, instead of using graphical models for post-processing after the ConvNets, the structure model can be combined into the feature learning layers of ConvNets, in the form of geometrical transform kernels, which can be implemented by convolutional layers. 

On the other hand, some methods tried to learn the spatial relationships between body parts in an implicit way. To achieve this, Tompson et al. 2014, Tompson et al. 2015, Rafi et al. 2016 learns multi-scale feature by using multi-resolution input passed into multi-branch networks. Some other researches tries to learn an implicit spatial model by composing a multi-stage network with each stage intermediately supervised, and using the output of the previous stage as the input for next stage. Wei et al. 2016 proposed a method called Convolutional Pose Machine (CPM), which incorporated convolutional neural network into the model called Pose Machine, and is able to learn multi-resolution feature by enlarging the receptive field through a multi-stage, very deep network, which is useful for infering the spatial relationship between body parts, as the receptive field in later layers are large enough to cover multple body parts and learn a good representation of the body structure. Similarly, Newell et al. proposed a stacked hourglass network composed of multiple bottom-up hourglass-like networks inspired by FCN for semantic segmentation. The stacked hourglass network is capable of extracting features of all scales and requires less computational cost due to the bottom-up structure, compared with CPM. The basic unit used in Newell et al. 2016 is the residual module proposed in He et al. 2016, which allows a deeper network and easier training process, and is used in many computer vision tasks. Many later works used stacked hourglass as a basic structure and extended it in various ways. To deal with the scale variations of body parts due to viewpoint change or severe foreshortening, a Pyramid Residual Module is proposed by Yang et al. 2017 to replace the residual module in stacked hourglass network, which obtains features of different scales via subsamping with different ratios. Besides, a initialization scheme was proposed for such multi-branch networks. Chu et al. proposed to incorporate multi-context attention mechanism into hourglass network by utilizing Conditional Random Fields to model correlations among neighbouring regions. The residual module was enhanced in their work with a side branch enlarging the receptive field, and is named Hourglass Residual Unit. 

Different from these approaches that initialize all joint predictions at once and iteratively refine the prediction, Gkioxari et al. 2016 propose to use the sequential model to predict one joint location at one time, where the prediction of each joint is dependent on all other previously predicted joints. 

### 3.3 Comparison and Hybrid Approach

For detection-based methods:
1. Better performance due to the rich information in heatmap. (+)
2. Non-differentiable argmax operation on the heatmap. (-)
3. The precision of predicted keypoints is proportional to that of the heatmap resolution (because the coordinates are discrete and have to be integers). Increased precision means increased memory space and time. (-)
4. Supervision on heatmap requires artificial groud truth generation. (-)

For regression-based methods:
1. Sub-optimal formulation of estimation-by-regression leads to unsatisfactory results. (-)
2. End-to-end and differentiable. (+)
3. Continuous output brings no quantization error. (+)
4. Can be used for both 2D and 3D human pose estimation. (+)

In order to alleviate the drawbacks of detection-based approach while keep the advantages, some hyrid methods are proposed that combines regression-based approach into detection-based approach. Bulat et al. 2016 use a detection subnet to learn the part heatmap, which learns local features and provides information about occluded body parts, and then a regression subnet to follow the guidance of part heatmap and learns holistic information to locate occluded parts. Differently, Luvizon et al. 2017 combines detection and regression into a single step, by replacing the taking-maximum operation with a taking-expection operation that is differentiable and produce continuous result, which requires no artificial ground truth generation. 

## 4. 2d multi-person pose estimation in images

For single-person pose estimation, it is often assumed that the location and scale of the human body is provided or the human body to be estimated takes up the whole image. However, in a more realistic settings, there can be an uncertain number of people in a image, with different and unknown scale and location, where the single-person pose estimation methods are not directly applicable. The major challenge for multi-person pose estimation in the wild stems from a higher potential for persons to fully or partially occlude the body parts of each other in the captured image. These mutual occlusions along with articulated nature of human body parts, makes it very hard for a single model to capture all the possible body-part appearances and spatial configurations in a multi-person image. Two different strategy has been used: estimating the pose of each individuals detected by a human body detector, which is often called top-down approach in the literature, and first detecting all body part candidates and then assembling them into different person instances, which is called bottom-up. 

### 4.1 Bottom-up approach

The bottom-up framework for multi-person pose estimation is also called part-based framework, which first detects all body parts independently and then assembles the parts to form multiple human poses. As regression-based networks requires the number of people in the image as a condition, they are seldom used for part detection. The advantages of bottom-up methods includes:

1. It has the potential to decoupling runtime complexity from the number of people in the image. 

1. It has robustness for early commitment problem in top-down approach and does not suffer from inaccurate or redundant bounding boxes. 

The disadvantages includes:

1. It sometimes loses the capability to recognize body parts from a global pose view and does not use global contextual cues from other body parts and other people. 

2. The body parsing stage are usually pretty time-consuming. 

The seminal work of Pishchulin et al. 2016 proposed a bottom-up approach that jointly labeled part detection candidates generated by an adapted Fast-RCNN, or a fully-connected VGG-based network predicting the part probability score maps, and jointly labels and partitions the candidate parts into individual poses using an ILP solver. However, ILP is a NP hard problem, which exhibits an exponential time complexity in the number of part-candidates.
Insafutdinov et al. 2016 built on this with stronger ResNet-based part detector and image dependent pairwise scores. The running time of ILP is reduced by 2~3 orders of magnitude by an incremental optimization strategy, but the method still takes several minutes per image with a limit on the number of part proposals. These ILP based methods (...) are NP hard and they becomes intractable as the number of part-candidates increases. 

A greedy part assignment algorithm that exploits the inherent structure of the human body to lower the complexity of the graphical model is proposed in Varadarajan et al. 2018. It sparsifying the body-part relationship graph to achieve a complexity which is linear in number of candidates of any single part-class, and use human anthropometric ratios for limiting the number of plausible person-clusters while assignment.

Following this path, Cao et al. 2017 propose to learn a bottom-up representation of pairwise relation called Part Affinity Fields (PAF), a set of 2d vector fields that encode the location and orientation of limbs. By jointly inferring the joint location and limb association, the global context is encoded sufficiently, and the proposed greedy parse is able to achieve improved result over state-of-the-art of the day, at almost real-time speed.  The person-clustering is formulated as a series of K-Partite graph-matching problems solved in parallel using the Hungarian algorithm, where K is the number of part-classes and the PAFs are used as edge-weights of this graph. Still, this method suffers from a limited structural context when the immediate predecessor is occluded.

Instead of trying to infer the pairwise relation between two nearby joints, Newell et al. proposed to make use of the unary relation between a detected body part and the group it belongs to. A joint heatmap together with pixel-wise embeddings which represent the group tags are learned and no complex post-processing is needed. The idea is relatively simple and allows for the use of any heatmap based methods except adding additional embedding score maps to the output and an additional loss function. Differently, Kocabas et al. 2018 claims to generalize Cao et al. 2017 and Newell et al. 2017, in the sense that the part assignment is achieved in a single shot by considering all joints together at the same time. Keypoint and person detection are performed in parallel, then a residual multi-layer perceptron takes them both as input and filters out part candidates not belonging to the person instance.

### 4.2 Top-down approach

The top-down framework is also called two-step framework, where a person instance detection method is first applied to get the bounding boxes, and then the single-person pose estimation step is performed on each cropped person patch. The advantages of these methods include:

1. existing off-the-shelf object detectors and single-person pose estimation methods can be utilized directly or adapted. For example, the combination of YOLO detector and convolutional pose machine is a baseline appearing quite often. 

The disadvantages includes: 

1. The result of pose estimation is much affected by the quality of bounding boxes generated by the human body detector, and there is no chance for the single-person pose estimation step to refine the result if the bounding boxes is wrong. 

2. This approach is  prone to problems of limited spatial context when large number of people overlap.

3. Single-person pose estimation is performed for each bounding boxes, which means the time cost will increase linearly according to the number of person in the image. 

#### 4.2.1 Human Detection

This step involves detecting the bounding boxes of human instances and cropping the image patchs. State-of-the-art object detectors are utilized in this step and we briefly reviewed this as not much attention has been paid on this considering this belongs to another research field. Human detection approaches are mainly guided by detectors in the RCNN family, which are two-stage detectors composed of proposal generation as well as proposal classification and refinement. In the work of Papandreou et al. 2017 and the Mask-RCNN (He et al. 2017), Faster-RCNN is used, which has a ResNet-101 network pretrained on Imagenet as backbone. Feature Pyramid Network, which is an extension of Faster-RCNN, is used in the work of Chen et al. 2018. The current state-of-the-art in the RCNN family is FPN. One-stage detectors are used in Fang et al. 2017 to balance the efficiency and performance as they are usually better in speed but worse in accuracy compared with the RCNN family detectors, but as the performance of top-down approaches is strongly connected to the quality of bounding boxes, these object detectors are relatively a second choice. Note that the detected human proposals are sometimes extended to guarantee the whole person region will be extracted.

#### 4.2.2 Person Pose Estimation

The second step in the top-down approach usually performs single-person pose estimation on the cropped patches of person instances. Some of them tries to make improvement on the work of Pishchulin et al. 2016 and Insafutdinov et al. 2016 by solving the ILP on single person instance patches. An approach aimed at reducing the complexity of the ILP through local joint assignment is proposed in Iqbal et al. 2017. For each person instance the body part candidates are generated using CPM, then the part-labeling and part-assignment problems are solved through multiple ILPs, one ILP evaluated for each human bounding box region. It achieves a 50 times speed up compared to DeeperCut at the cost of poor accuracy. 
Insafutdinov et al. 2017 reduces the complexity of the graph-partitioning problem by taking a top down approach and solves a set of simplified graph-partitioning problems in local regions around the head-detections. The speed up is achieved by replacing ILP with a modified KL algorithm.

Papandreous et al. 2017 proposed to predict dense heatmaps and offsets to group truth on the cropped images using a fully-connected ResNet and then aggregate them by Hough voting to produce highly localized activation maps which precisely pinpoint the position of keypoints. 
In the work of He et al. 2017, a FCN-like branch predicting one-hot keypoint masks of the Region of Interest in parallel to the existing branch for classification and bounding box regression is added to the original Faster-RCNN network, and the keypoints inside each predicted bounding box are regarded as the corresponding keypoints of the person instance.
As the network tends to pay more attention to the "simple" joints of the majority but less to the "hard" joints caused by occlusion, invisibility or crowded background during training, to ensure the network balance between these two types of joints, Chen et al. 2018 proposed a two-stage cascaded pyramid network consisting of FPN-like network which learns good feature representation and estimate the "simple" keypoints, and a HyperNet-like network which utilizes the pyramid features for context information and address the "hard" joints based on the online hard keypoints mining loss. 

Noticing the fact that the quality of bounding boxes is important to the final pose estimate, Fang et al. 2017 proposed the RMPE framework to improve detection result in the later pose estimation stage. A symmetric spatial transformer network is attached to the single-person pose estimation module to extract high-quality single person region from an inaccurate bouding box, and a parametric pose NMS using a novel pose distance metric to compare pose similarity is introduced as a post-processing step to remove redundant poses caused by redundant bounding boxes. 

## 5. 2d Human Pose Estimation in videos

For 2d human pose estimation in videos, methods for single RGB images can be used directly or adapted. Notwithstanding the superior performance on static images, the application of these models on videos is not only computationally intensive, but also suffers from performance degeneration and flicking. Such suboptimal results are mainly attributed to the inability of imposing sequential geometric consistency, handling severe image quality degradation (e.g. motion blur and occlusion) as well as the inability of capturing the temporal correlation among video frames.

Human pose estimation in videos always includes temporal cues to reflect the relationship of the same body parts in adjacent frames, which is helpful to solve the problem brought by occlusion and articulation. To think about this in a simple way, even if the current frame is hard for pose estimation, it may be relatively easy on the frames before or after, and as the pose cannot change too much in consecutive frames due to temporal consistency, the pose in current frame can be infered based on neighbouring frames, which lowers down the difficulty but brings new challenges on how to utilize the temporal information in videos. 

To our knowledge, Pfister et al. 2014 proposed the first method to use ConvNet for human pose estimation in videos, which makes the attempt to use multiple frames stacked together as input. A network for object detetion, named Overfeat, is used as the basic structure and adapted to directly regress the 2d joint coordinates. An heatmap-based approach is proposed by Jain et al. 2014, which use both an single RGB frame and its corresponding motion feature (for example, image difference or optical flow) patch as input.
Comparatively, Pfister et al. 2015 uses optical flow to warp heatmaps of multiple neighbouring frames, rather than using them as direct input to the ConvNet, and combines these heatmaps with a learned 1X1 conv layer performing spatial pooling. Futher more, Charles et al. 2016 propose to use additional high-quality annotations generated throughout the video using a combination of image-based matching and dense optical flow to fine-tune a ConvNet pose estimator and personlize it to lock on to key discriminative features of the person's appearance. These methods above has only been tested on upper-body pose datasets. 

Among the most dominant approaches to pose estimation from videos is a two-stage approach, which first deploys a frame-level keypoint estimator, and then connects these keypoints using spatial-temporal graph model and optimization techniques. Similar to Pfister et al. 2015, Song et al. 2017 warp the part heatmaps generated by ConvNet with ConvNet, but put both the warped and unwarped maps into a CRF layer with space-time edges and jointly optimize for the pose predictions. But their approach is still hard to generalize to an unknown number of person instances, a number that might vary even between consecutive frames due to occlusions and disocclusions. In Insafutdinov et al. 2017 and Iqbal et al. 2017, it is shown that a state-of-the-art pose detector followed by an integer programming optimization problem can partition the graph into valid body pose trajectories for an unknown number of persons, and result in very competitive performance in complex videos. While these approaches can handle both space-time smoothing and identity assignment, they are not applicable to long videos, for the ILP optimization is NP-hard.

RNN is also used to capture temporal dependencies in human pose estimation in videos, as in Gkioxari et al. 2016 and Luo et al. 2018. Gkioxari et al. 2016 propose to use a chain model wth weight sharing to enforce the pose recurrence in video, which is a convolutional RNN where the output variables of the previous steps are used for current step. Luo et al. 2018 rewrite the multi-stage CNN proposed by Wei et al. 2016 that is widely used for human pose estimation on images as a RCNN by imposing weight sharing scheme, and adopt the memory augmented RNN (LSTM) to better capture temporal information and get better result compared with Gkioxari et al. 2016.


## 6. 3d human pose estimation

In recent years, as 3D data have been widely used in HPE field and the hardware for capturing 3D human joints have developed, 3D human pose estimation becomes a promising technique area. To get the 3D information is the key in 3D human parsing and the 3D information can be captured either from the multi-view images/video or from the depth information in monocular image. The techniques used are quite different. Here we focus on infering the depth information from monocular images or videos. 

The reconstruction of an arbitrary configuration of 3D points from a single monocular RGB image has three characteristics that affect its performance: (i) it is a severely ill-posed problem because similar 2d image projections can be derived from different 3D poses; (ii) it is an ill-conditioned problem since minor errors in the locations of the 2D body joints can have large consequences in the 3D space; and (iii) it suffers from high dimensionality. In essense, the difficulty lies in the fact that input (RGB image) is less than the output (3d coordinates) because the image does not have the direct depth information. For 3d reconstruction, a simple image should not be sufficient to get the depth data. However, for the specific domain of 3d human pose estimation, keypoints are not independent to each other and they actually have very strong dependencies, which can be made use of to solve this problem. The most direct and effective evidence for this is that human is capable of inferring the 3d pose from a single RGB image.

Currently there exist two approaches for 3d human pose estimation: the two-stage approach that first output the 2d coordinates of body parts and then use optimization algorithms or machine learning methods to "lift" the pose from 2d space to 3d space; the end-to-end approach using ConvNets to take in images and output the 3d coordinates directly or the probablity heatmap. These two approaches will be reviewed separately in section 6.1 and 6.2.

Still, advance in 3D human pose estimation in the wild remains limited. This is partially due to the ambiguity of recovering 3D information from single images, and partially due to the lack of large scale 3D pose annotation dataset. Specifically, there is not yet a comprehensive 3D human pose dataset for images in the wild. The commonly used 3D datasets like Human3.6m and HumanEva were captured by mocap systems in controlled lab environments. Deep neural networks trained on these datasets do not generalize well to more complex in-the-wild environments. The two-step approach is considered suitable to address the domain difference between 3D human pose estimation datasets and images in the wild. An alternative approach for 3D human pose estimation is to train from synthetic datasets, which will be reviewed in section 3.

### 6.1 End-to-end Approach

3D human pose estimation, like the 2d situation, can be formulated as a standard supervised-learning problem. The majority of recent ConvNet-only approaches cast 3D pose estimation as a coordinate regression task, with the target output being the spatial x, y, z coordinates of the human joints with respect to a known root joint, such as the pelvis. CNN is able to learn useful features directly from images without keypoint matching step in the typical 3D reconstruction tasks.

Li and Chan 2014 proposed to train a deep convolutional neural network to directly regress joint locations. To be more accurate, the proposed method is a multi-tasking network which performs joint point regression to estimate joint positions relative to their parent joints, and joint point detection to classify whether one local window contains the specific joint. They show that the network in its last layers has an internal representation for the positions of the left (or right) side of the person, and thus, has learned the structure of the skeleton and the correlation between output variables.

Recently, people have generalized this approach in different directions. 
Zhou et al. 2016 propose to explicitly enforce the bone-length constraints in the prediction, using a generative forward-kinematic layer; 
Sun et al. 2017 adopt a bone-based pose representation similar to Li and Chan 2014 that represents each joint locally with regard to its parent joint in a kinematic tree and better captures body part dependencies, and propose a compositional loss function to resolove the side-effect of long range error accumulation brounght by the bone representation. 
In the work of Tekin et al. 2016 (BMVC), To encode dependencies between joint locations, an auto-encoder is trained on existing human poses to learn a structured latent representation of the human pose in 3D. Following that, a ConvNet architecture maps through a regression framework the input image to the latent representation and the decoding layer is then used to estimate the 3D pose from the latent to the original 3D space.
Ghezelghieh et al. 2016 employ viewpoint prediction as a side task to provide the network with global joint configuration information. 
To use 2d pose information to help avoiding undisirable 3d joint positions generating unnatural pose, Park et al. 2016 added 2D information to estimate a 3D pose from an image by concatenating 2D pose estimation result with the features from an image, and obtained more accurate 3D poses by combining information on relative positions with respect to multiple joints, instead of just one root joint.
Du et al. 2016 combined both RGB image and its calculated height-map to detect the landmarks of 2D joints with a dual-stream ConvNet. They formulate a new objective function to estimate 3D motion from the detected 2D joints in the monocular image sequence, which reinforces the temporal coherence constraints on both the camera and 3D poses.

Li et al. (2015) proposed a framework which takes as an input an image and a 3D pose and produces a score value that represents a multi-view similarity between the two inputs (i.e., whether they depict the same pose). A ConvNet for feature extraction is employed and two sub-networks are used to perform a non-linear transformation of the image and pose into a joint embedding. A maximum-margin cost function is used during training which enforces a re-scaling margin between the score values of the ground truth image-pose pair and the rest image-pose pairs. The score function is the dot-product between the two embeddings. 

In contrast to these works, Pavlakos et al. 2017 adopt a 3D volumetric representation of the human pose, and regress the per voxel likelihood for each joint separately, like the detection-based methods for 2d human pose estimation that regress an 2d pixel-wise likehood for each joint. The advantages of this method includes: 
1. ConvNets can naturally map 2D images to 3D volumes and this is easier compared with the mapping from 2D images to 3D coordinates, which is highly non-linear. 
2. The mapping can be achieved with a fully convolutional network and non-FC layers, which means the number of parameters can be reduced a lot. 
3. The output is relatively rich compared with direct regression methods, and amenable to other tasks, or pose-processing like pictorial structures optimization to constrain limb lengths or temporal filtering.


<!-- mehta et al. 2018 novel occlusion-robust pose-maps (ORPM) -->


### 6.2 Two-stage Approach

Despite the interest in end-to-end learning, most of the ConvNet-only methods reviewed in the former section underperform those two-stage methods that explore the power of decoupling 3d pose estimation into the well studied problems of 2d pose estimation (...), and 3d pose estimation from 2d joint detections, focusing on the latter. 
Separating pose estimation into these two problems gives us the possibility of exploiting existing off-the-shelf 2d pose estimatiors which already provide invariance to the previously mentioned factors like lighting, apperance variance and self-occlusion.
Another advantage is the ability to adapt to different environment, whether in-the-wild or in-the-lab. The first sub-task can use various 2d datasets of in-the-wild images for training. For the second step, since the input is just a set of 2D locations, the 3D pose estimation network can be trained on any 3d benchmark datasets and then adapted in other settings. Data-hungry algorithms for the 2d-to-3d problem can be trained with large amounts of 3d mocap data captured in controlled environments, while working with low-dimensional representations that scale well with large amounts of data.
Still, a common limitation of these two-stage approaches is that the 3D pose is only estimated from the 2D joints, which is known to produce ambiguous result due to the ill-posed nature of 2d to 3d problems mentioned above in section 6.

<!-- bogo et al. 2016 -->
tekin et al. 2016 bmvc need to add.
<!-- tome 2017 pretrained probabilistic 3d model layer, 3d to 2d; -->
<!-- martinez 2017 simple network; -->
<!-- chen 2017 nearest neighbour; -->
<!-- moreno-noguer 2017 -->

<!-- Yasin et al. 2016 proposed a dual-source approach that is able to incorporate 2d and 3d information from two different training sources, of which one consists of images with annotated 2d pose and is used for the 2d joint localization sub-task and the other is accurate 3d motion capture data captured in the lab and is used for 2d to 3d inference sub-task.  -->
<!-- no deep learning -->

To lift the pose from 2d space to 3d space, various methods have been proposed.
Bogo et al. 2016 use the 2d joints predicted by the 2d joint estimator DeepCut in a bottom-up way and then fit a recently published statistical body shape model SPL to the 2D joints. This is achieved by minimizing an objective function that penalizes the error between the projected 3d model joints and detected 2d joints. The model is able to be robustly fit to very little data because SMPL is able to capture correlation in human shapes. 
Chen et al. 2017 use nearest-neighbor search to match the estimated 2D pose to a 3D pose as well as a camera-view which may produce a similar 2D projection from a large 3D pose library.
Tome et al. 2017 propose to use a pre-trained probabilistic 3D pose model that first generates plausible 3D human pose from 2D heat-maps, and fuse the 3d pose projection with the 2d heatmap to reasons jointly in 2d and 3d space to improve both tasks. The probablistic 3D pose model can be embedded as an additional layer and the network is trained in an end-to-end fashion. 


<!-- need to add here -->

ConvNets were also employed in a deep learning regression architecture work of Tekin et al. 2016 BMVC . To encode dependencies between joint locations, an auto-encoder is trained on existing human poses to learn a structured latent representation of the human pose in 3D. Following that, a ConvNet architecture maps through a regression framework the input image to the latent representation and the decoding layer is then used to estimate the 3D pose from the latent to the original 3D space.


Mehta et al. 2017 propose a real-time kinematic skeleton fitting method uses the CNN output to yield temporally stable 3D global pose reconstructions on the basis of a coherent kinematic skeleton. Their approach is claimed to be the first monocular RGB method usable in real-time applications

<!-- network based -->
Some other methods use neural networks to carry out the 2d-to-3d mapping process.
Instead of performing the 2D-to-3D inference in Cartesian space, between 2N and 3N vector representations of the N body joints, Moreno-Noguer 2017 proposed to  represent 2D and 3D poses using N X N matrices of Euclidean distances between every pair of joints, and formulate the 3D pose estimation problem as one of 2D-to-3D distance matrix regression. Distance matrices are invariant up to rotation, translation and reflection; therefore, multi-dimensional scaling is complemented with a prior of human poses (Akhter et al. 2015) to rule out unlikely predictions. A simple neural network is leveraged for learning such a regressor, which by construction, enforce positivity and symmetry of the predicted matrices. The approach has also the advantage to naturally handle missing observations and allowing to hypothesize the position of non-observed joints.
Martinez et al. 2017 propose a simple but effective baseline for the second step via a simple feed foward network. The network architecture is simple, typically consisting of a linear layer which takes the input dimensionality to 1024, a series of two residual blocks (which themselves involve two linear layers with subsequent batch norms, ReLUs, and dropout, and a residual connection from the block input to block output), and finally a linear layer mapping to the output dimensionality. It contradicts the idea that regressing 3d keypoints from 2d joint detections directly should be avoided, and shows that a well-designed and simple network can perform quite competitively in the task of 2d-to-3d keypoint regression.

<!-- This shows that lifting 2d poses is, although far from solved, an easier task than previously thought. Since our work also achieves state-of-the-art results starting from the output of an off-the-shelf 2d detector, it also suggests that current systems could be further improved by focusing on the visual parsing of human bodies in 2d images. -->

<!-- The recovery of 3D human poses in monocular images is a difficult task in computer vision since highly nonlinear human motions, pose and appearance variance, cluttered backgrounds, occlusions (both from other people or objects and self-occlusions), and the ambiguity between 2D and 3D poses are common phenomena. -->

<!-- addtional prior knowledge: -->
<!-- depth ranking wang et al. 2018 -->
<!-- height map -->

There also exists some methods that try to add some built-in prior knowledge into the framework to help infer the 3d location of body parts, especially the depth information. 
Du et al. 2016 introduce the height-map calculated from images for the reconstructing of 3d pose. However, the height-map is only combined with images to detect the landmarks of 2D joints with a dual-stream deep convolution network, and not used in the 2d-to-3d process, which lacks further experiment evidence about whether or not the height-map is able to provide 3d information instead of only 2d appearance information. 
Differently, Wang et al. 2018 extract depth information directly from the image evidence. A network is proposed to regress an 2d pairwise ranking matrix of the depth orders between body parts from the 2d heatmaps. Under the assumption that the distance between a pair of body parts are fixed, a multi-stage network for the second stage is able to take the 2d heatmap together with the pairwise ranking matrix as input and regress the 3d pose in a coarse-to-fine way.

<!-- time information -->
<!-- sparseness meets deepness -->
<!-- height map -->

In addition to image-based methods, some video-based methods try to leverage the temporal constraints between video frames to help infering 3d pose.



Tekin et al. 2016 CVPR used multiple consecutive frames to build a spatio-temporal features, and the features are fed to a deep neural network regressor to estimate the 3D pose. Spatiotemporal information is exploited to reduce depth ambiguities. They employ 2 ConvNets to first align (i.e., shifting to compensate for the motion) the bounding boxes of the human in consecutive frames and then refine them so as to create a data volume. 3D HoG descriptors are computed and the 3D pose is reconstructed directly from the volume with Kernel Ridge Regression (KRR) and Kernel Dependency Estimation (KDE). They demonstrated that (i) when information from multiple frames is exploited, challenging ambiguous poses where self-occlusion occurs can be estimated more accurately and (ii) the linking of detections in individual frames in an early stage, followed by enforcing temporal consistency at a later stage improves the performance significantly.
Human detection in multiple frames and motion compensation to form a spatiotemporal volume with two ConvNets; 3D HoGs are employed and 3D pose is estimated with KRR and KDE regression.



In the work of Zhou et al. 2015, using the 2D image landmarks as an input, they adopted an augmented shape-space model to give a linear representation of both intrinsic shape deformation and extrinsic viewpoint changes. They proposed a convex formulation that guarantees global optimality and solved the optimization problem with a novel algorithm based on the Alternating Direction Method of Multipliers (ADMM) and the proximal operator of the spectral norm. Actually, their method is applicable not only to human pose but also to car and face reconstruction.


### 6.3 Data Synthesizing

the lack of training data for ConvNet-based techniques remains a significant challenge. Towards this direction, the methods of Chen et al. 2016 and Rogez and Schmid 2016 propose techniques to synthesize training images with ground truth pose annotations and train from synthetic datasets which are generated from deforming a human template model with known 3D ground truth (Chen et al. 3DV 2016, Rogez and Schmid NIPS 2016). This is indeed a viable solution, but the fundamental challenge is how to model the 3D environment so that the distribution of the synthesized images matches that of the natural images. It turns out state-of-the-art methods along this line are less competitive on natural images. 

Chen et al. use a graphics renderer to create images with known groundtruth. Similarly, Ghezelghieh et al. augment the training set with synthesized examples. 

A collage approach is proposed by Rogez and Schmid 2017, where parts from in-the-wild images are combined to create additional images with known 3D poses. However, there is no guarantee that the statistics of the synthetic examples match those of real images.

rogez and schmid 2016 augments images by assembling multiple images according to 3d poses, but the synthetic images contain a lot of artifacts.


<!-- (Wu et al. 2016 ...) -->

<!-- Finally, the task of estimating the 3D human pose from image sequences has also been explored using deep learning, Elhayek et al. (2015, 2016); Hong et al. (2015); Tekin et al. (2016a, 2016b); Zhou et al. (2016b) and the respective methods are going to be discussed individually in Sections 4.1 and 4.2. -->

Zhou et al. 2016 utilize a standard 2D pose ConvNet to localize the joints and retrieve the 3D pose using an optimization scheme over a sequence of monocular images. If 2D joints are provided, a sparsity-driven 3D geometric prior and temporal smoothness model are employed; If not, 2D joints are treated as latent variables and a deep ConvNet is trained to predict the uncertainty maps; 3D pose estimation is realized via an EM algorithm over the entire sequence to compute a 3D skeleton by combining a sparse dictionary induced from the 2D heat-maps.

<!-- They enforces temporal consistency by matching video trajectories to a spatio-temporal 3D model, which provides robustness to viewpoint changes.  -->
<!-- Zhou et al. [19] used the result of 2D pose estimation to reconstruct a 3D pose. They represented a 3D pose as a weighted sum of shape bases similar to typical non-rigid structure from motion, and they designed an EM-algorithm which formulates the 3D pose as a latent variable when 2D pose estimation results are available. The method achieved the state-of-the-art performance for 3D human pose estimation when combined with 2D pose predictions learned from CNN.  -->


## 7. Benchmarks

### 7.1 Evaluation Metrics

### 7.2 Datasets

In this part we try to cover most existing datasets for human pose estimation. Basic information for these datasets are introduced in the table below. Note that LSP and FLIC datasets used to be the standard benchmarks for 2d single-person pose estimation, but soon replaced by MPII single-person dataset, on which the performance is also closely saturated, leaving 2d single-person pose estimation dataset close to be solved. The widely used benchmark for 2d multi-person pose estimation is MS COCO.

## 8. Future Trends and Conclusion

### 8.1 Future Trends

#### 8.1.1 Person Re-identification

#### 8.1.2 Human Pose Tracking 

PoseTrack Challenge, etc.

### 8.2 Conclusion